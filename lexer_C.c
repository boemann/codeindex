#include <exec/types.h>


#include <proto/exec.h>
#include <proto/dos.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <containerkit/map.h>
#include <clib/containerkit_protos.h>
#include <pragmas/containerkit_pragmas.h>

#include "symdb.h"
#include "lvo_clib.h"

#include "lexer_C.h"
#include "ppEval.h"

struct Lexer
{
	STRPTR contents;
	STRPTR contentsEnd;
	STRPTR cursor;
	STRPTR BOL;
	ULONG blockNum;
	STRPTR symbolTable;
	STRPTR nextFreeSymbol;
	STRPTR symbolInsertion;
	Map	keywordMap;
	Map preKeywordMap;
	LONG ppSkipDepth;
	LONG ppDepth;
	LONG ppCondition[32];
	APTR sdb;
	SymbolRef fileScope;
	Array definedTextArray;
	STRPTR stashedContentsEnd;
	STRPTR stashedCursor;
	STRPTR stashedContents;
	BOOL haveStash;
};

// these are defined at the bottom of this file
extern UWORD preDetermine[256];
extern UBYTE followupConv[];
extern enum TokenKind followupToToken[][10];

extern struct Library *ContainerkitBase;

struct Lexer *NewLexer(STRPTR contents, APTR sdb, SymbolRef fileScope)
{
	struct Lexer *lexer = AllocMem(sizeof(struct Lexer), MEMF_CLEAR);
	
	if (!lexer)
		return NULL;
	
	lexer->contents	= contents;
	lexer->contentsEnd = lexer->contents + strlen(lexer->contents) + 1;
	lexer->cursor =	lexer->contents;
	lexer->blockNum	= 0;
	lexer->symbolTable = AllocVec(lexer->contentsEnd-lexer->contents, MEMF_ANY);
	lexer->nextFreeSymbol =	lexer->symbolInsertion = lexer->symbolTable;
	lexer->BOL = lexer->cursor;
	lexer->sdb = sdb;
	lexer->fileScope = fileScope;
	lexer->definedTextArray = NewArray(0);

	
	lexer->keywordMap =	NewMap(CNTKIT_KEY_STRING, CNTKIT_VALUESIZE,	4, TAG_DONE);

	MapStrToValue(lexer->keywordMap, "const", TOKEN_CONST);
	MapStrToValue(lexer->keywordMap, "register", TOKEN_CONST);
	MapStrToValue(lexer->keywordMap, "static", TOKEN_CONST);
	MapStrToValue(lexer->keywordMap, "extern", TOKEN_CONST);
	MapStrToValue(lexer->keywordMap, "signed", TOKEN_UNSIGNED); // unsigned on purpose as it simplifies other code
	MapStrToValue(lexer->keywordMap, "unsigned", TOKEN_UNSIGNED);
	MapStrToValue(lexer->keywordMap, "void", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "char", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "int",	TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "long", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "short", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "float", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "double", TOKEN_SCALARTYPE);
	MapStrToValue(lexer->keywordMap, "struct", TOKEN_STRUCTORUNION);
	MapStrToValue(lexer->keywordMap, "union", TOKEN_STRUCTORUNION);
	MapStrToValue(lexer->keywordMap, "enum", TOKEN_ENUM);
	MapStrToValue(lexer->keywordMap, "typedef",	TOKEN_TYPEDEF);
	MapStrToValue(lexer->keywordMap, "if", TOKEN_CONTROLSTATEMENT);
	MapStrToValue(lexer->keywordMap, "while", TOKEN_CONTROLSTATEMENT);
	MapStrToValue(lexer->keywordMap, "do", TOKEN_DO);
	MapStrToValue(lexer->keywordMap, "for",	TOKEN_CONTROLSTATEMENT);
	MapStrToValue(lexer->keywordMap, "return", TOKEN_INVALID);
	MapStrToValue(lexer->keywordMap, "switch", TOKEN_INVALID);
	MapStrToValue(lexer->keywordMap, "case", TOKEN_INVALID);
	MapStrToValue(lexer->keywordMap, "default", TOKEN_INVALID);
	MapStrToValue(lexer->keywordMap, "for", TOKEN_INVALID);

    lexer->preKeywordMap = NewMap(CNTKIT_KEY_STRING, CNTKIT_VALUESIZE, 4, TAG_DONE);

	MapStrToValue(lexer->preKeywordMap,	"define", TOKEN_DEFINE);
	MapStrToValue(lexer->preKeywordMap,	"include", TOKEN_INCLUDE);
	MapStrToValue(lexer->preKeywordMap,	"ifdef", PPKIND_CONTROL|PPKIND_INCLEVEL|PPKIND_EVALUATE|PPKIND_EVALDEF);
	MapStrToValue(lexer->preKeywordMap,	"ifndef", PPKIND_CONTROL|PPKIND_INCLEVEL|PPKIND_EVALUATE|PPKIND_EVALDEF|PPKIND_NEGEVAL);
	MapStrToValue(lexer->preKeywordMap,	"if", PPKIND_CONTROL|PPKIND_INCLEVEL|PPKIND_EVALUATE|PPKIND_EVALEXPR);
	MapStrToValue(lexer->preKeywordMap,	"elif", PPKIND_CONTROL|PPKIND_EVALUATE|PPKIND_ELSE|PPKIND_EVALEXPR);
	MapStrToValue(lexer->preKeywordMap,	"else", PPKIND_CONTROL|PPKIND_EVALUATE|PPKIND_ELSE);
	MapStrToValue(lexer->preKeywordMap,	"endif", PPKIND_CONTROL|PPKIND_DECLEVEL);

	lexer->ppSkipDepth = 10000;
	
	return lexer;
}

void DeleteLexer(struct Lexer *lexer)
{
	DeleteArray(lexer->definedTextArray);
	DeleteMap(lexer->preKeywordMap);
	DeleteMap(lexer->keywordMap);
	FreeVec(lexer->symbolTable);
	FreeMem(lexer, sizeof(struct Lexer));
}

ULONG LexerGetBlockNum(struct Lexer *lexer)
{
	return lexer->blockNum;
}

static void LexerAdvanceBlock(struct Lexer *lexer)
{
	lexer->blockNum++;
	lexer->BOL = lexer->cursor;
}

static void LexerInjectDefineValue(struct Lexer *lexer, STRPTR pos, STRPTR newText)
{
	int index = pos - lexer->contents;
	int oldLength = lexer->cursor - pos;
	int newLength = strlen(newText);
	
	if (! lexer->haveStash)
	{
		lexer->stashedContents = lexer->contents;
		lexer->stashedContentsEnd = lexer->contentsEnd;
		lexer->stashedCursor = lexer->cursor;
		lexer->haveStash = TRUE;

		index = 0;
		oldLength = 0;
	}
	
	if (oldLength < newLength)
		InsertArrayElements(lexer->definedTextArray, index, newLength - oldLength);
	else
		EraseArrayElements(lexer->definedTextArray, index, oldLength - newLength);
		
	// addresss might've changed
	lexer->contents = ArrayValues(UBYTE, lexer->definedTextArray);
	lexer->contentsEnd = lexer->contents + SizeOfArray(lexer->definedTextArray);
	
	CopyMem(newText, lexer->contents + index, newLength);
	
	lexer->cursor = lexer->contents + index;
}

Map CreateDefinitionArgMap(struct Lexer *lexer, SymbolRef defRef)
{
	STRPTR args = GetDefinitionArgs(lexer->sdb, defRef);
	TEXT argTerminator;

	if (args && *lexer->cursor == '(')
	{
		int parDepth = 1;
		Map argMap = NULL;
		STRPTR argName;
		STRPTR argValue;
		
		argMap = NewMap(CNTKIT_KEY_STRING, CNTKIT_VALUESIZE, 4, TAG_DONE);

		argValue = lexer->cursor + 1;
		
		argName = args;
		while (*args && *args != ',')
			args++;
		argTerminator = *args;
		*args= '\0';
		while (parDepth)
		{
			lexer->cursor++;
			if (*lexer->cursor == '(')
				parDepth++;
			if (*lexer->cursor == ')')
				parDepth--;
				
			if (parDepth == 0 ||
			 (parDepth == 1 && *lexer->cursor == ','))
			{
				// End of argument reached
				
				*lexer->cursor = '\0';
				
				MapStrToValue(argMap, argName, (ULONG)argValue);

				argValue = lexer->cursor + 1; // next argValue
				
				// Find the next argName
				*args = argTerminator;
				if (argTerminator)
					args++; // skip the comma between arg names
				argName = args;
				while (*args && *args != ',')
					args++;
				argTerminator = *args;
				*args= '\0';
			}
		}
		lexer->cursor++;
		
		return argMap;	
	}
	return NULL;
}

static BOOL LexerResolveDefinition(struct Lexer *lexer,
 STRPTR pos, SymbolRef defRef)
{
	STRPTR body;
	Map argMap;

	body = GetDefinitionValue(lexer->sdb, defRef);

	if (!body)
		return FALSE;

	argMap = CreateDefinitionArgMap(lexer, defRef);
	
	if (argMap)
	{
		Array newText = NewArray(0);
		STRPTR in = body;
		
		while (*in)
		{
			BOOL convertSymToString = (*in == '#');
			
			if (convertSymToString)
			{
				in++;
				if (*in == '#')
				{
					// It is actually a concatination
					// Let's remove spaces before
					STRPTR previousNonSpace = in - 2;

					while (previousNonSpace > body && isspace(*previousNonSpace))
						previousNonSpace--;
					ResizeArray(newText, previousNonSpace - body + 1);

					// And make sure we don't output spaces after						
					in++;
					while (isspace(*in))
						in++;

					convertSymToString = FALSE; // and obviously not a convert
				}
				
				if (! *in)
					break;
			}
			
			if (*in == '_' || isalpha(*in))
			{
				STRPTR replacement;
				STRPTR symStart = in;
				TEXT c;
					
				in++;
				while (*in == '_' || isalnum(*in))
					in++;
				
				// now in will point after the symbol
				c = *in;
				*in = '\0';
				
				replacement = (STRPTR)ValueOfStrKey(argMap, symStart);
				
				if (replacement)
				{
					if (convertSymToString)
						AppendToArray(TEXT, newText, '"');

					while (*replacement)
						AppendToArray(TEXT, newText, *replacement++);

					if (convertSymToString)
						AppendToArray(TEXT, newText, '"');
				}
				else
					while (symStart != in)
						AppendToArray(TEXT, newText, *symStart++);
					
				*in = c;
				continue;				
			}
			if (isdigit(*in))
			{
				while (isdigit(*in))
					AppendToArray(TEXT, newText, *in++);
				continue;				
			}

			AppendToArray(TEXT, newText, *in++);
		}
		AppendToArray(TEXT, newText, '\0');
		
		LexerInjectDefineValue(lexer, pos, ArrayValues(TEXT, newText));

		DeleteArray(newText);
		DeleteMap(argMap);
	}
	else
		LexerInjectDefineValue(lexer, pos, body);
	
	return TRUE;
}

static int LexerCompleteResolvingDefines(struct Lexer *lexer)
{
	if (! lexer->haveStash)
		return FALSE;
		
	lexer->contentsEnd = lexer->stashedContentsEnd;
	lexer->cursor = lexer->stashedCursor;
	ResizeArray(lexer->definedTextArray, 0);
	lexer->haveStash = FALSE;
	return TRUE;
}

static TEXT skipComment(struct Lexer *lexer, int comment)
{
	lexer->cursor += 2;
	while (lexer->cursor < lexer->contentsEnd)
	{
		if (*lexer->cursor == '\n')
		{
			if (comment == 1)
				break;
			else
				LexerAdvanceBlock(lexer);
		}
		else if (comment == 2 && *lexer->cursor == '*')
		{
			lexer->cursor++;
			if (*lexer->cursor == '/')
			{
				lexer->cursor++;
				break;
			}
		}
		lexer->cursor++;
	}
	return *lexer->cursor;
;}

static void	LexerJumpAfterNextHash(struct Lexer *lexer)
{
	char c;
	int afterNL = TRUE;

	c = *lexer->cursor;
	
	while (lexer->cursor < lexer->contentsEnd)
	{
		if (c == '/' && lexer->cursor[1] == '*')
			// no need to handle // comments
			c = skipComment(lexer, 2);
			
		if (c == '\n')
			LexerAdvanceBlock(lexer);
			
		lexer->cursor++;

		if (afterNL && c == '#')
			return;
			
		afterNL = (c == '\n');
	
		c = *lexer->cursor;
	}
}

int trimText(STRPTR text)
{
	STRPTR end = text + strlen(text) - 1;
	STRPTR cptr = text;
	int numTrimmed = 0;
	
	// trim away whitespace at the end
	while (end >= text)
	{
		if (!isspace(*end))
			break;

		*end = 0;
		numTrimmed++;
		end--;
	}
	
	// figure out how much to remove from the beginning
	while (*cptr && isspace(*cptr))
		cptr++;
		
	numTrimmed += cptr-text;
	
	// move the text to the beginning
	while (*cptr)
		*text++ = *cptr++;
	*text = 0;
	
	return numTrimmed;
}

BOOL spanIsOnlyWhite(STRPTR start, STRPTR afterEnd)
{
	while (start != afterEnd)
	{
		if (!isspace(*start))
			return FALSE;
		start++;
	}
	return TRUE;
}

struct Token LexerNext(struct Lexer *lexer)
{
	struct Token token;
	char c;	

start:

	c = *lexer->cursor;

	token.kind = preDetermine[c];

	// let's strip off spacing and invalids
	while (lexer->cursor < lexer->contentsEnd
			&& (token.kind == TOKEN_INVALID))
	{
		lexer->cursor++;

		if (c == '\n')
			LexerAdvanceBlock(lexer);

		c = *lexer->cursor;
		token.kind = preDetermine[c];
	}

	
	token.numChars = 0;
	token.blockNum = lexer->blockNum;
	
	if (lexer->cursor >= lexer->contentsEnd)
	{
		if (LexerCompleteResolvingDefines(lexer))
			goto start;
			
		token.kind = TOKEN_END;
		return token;
	}
		
	// from above predetermine we already know what token kind to expect

//	if (token.kind == TOKEN_HASH && lexer->BOL == lexer->cursor)
	if (token.kind == TOKEN_HASH && spanIsOnlyWhite(lexer->BOL, lexer->cursor))
	{
		STRPTR ppCmd = lexer->cursor + 1;
		int ignoreNL = FALSE;
		int collectStr = FALSE;
		
		token.text = lexer->nextFreeSymbol;

		while (lexer->cursor < lexer->contentsEnd)
		{
			if (collectStr)
			{
				LONG comment = 0;
				if (c == '/')
				{
					if (lexer->cursor[1] == '/')
						comment = 1;
					else if (lexer->cursor[1] == '*')
						comment = 2;
				}

				if (comment)
					// cpp comment so skip until newline
					c = skipComment(lexer, comment);

				*lexer->symbolInsertion++ = c;
			}
			else if (isspace(c))
			{
				*lexer->cursor = 0; // temp terminate string

				token.kind = ValueOfStrKey(lexer->preKeywordMap, ppCmd);

				*lexer->cursor = c; // restore
				
				token.text = lexer->nextFreeSymbol;
				collectStr = TRUE;
			}

			lexer->cursor++;
			
			if (c == '\n')
			{
				LexerAdvanceBlock(lexer);
				
				if (!ignoreNL)
				{
					if (collectStr)
					{
						*lexer->symbolInsertion++ = 0;
						lexer->symbolInsertion -= trimText(token.text);
						lexer->nextFreeSymbol = lexer->symbolInsertion;
						collectStr = FALSE;
					}
				
					if (token.kind == 0)
						token.kind = TOKEN_HASH;
					else if (token.kind & PPKIND_CONTROL)
					{
						if (token.kind & PPKIND_INCLEVEL)
							lexer->ppDepth++;
						if (token.kind & PPKIND_DECLEVEL)
							lexer->ppDepth--;
							
						if ((lexer->ppDepth < lexer->ppSkipDepth)
							|| (lexer->ppSkipDepth == lexer->ppDepth
								&& token.kind & PPKIND_ELSE))
						{
							if (token.kind & PPKIND_EVALUATE)
							{
								BOOL flipBack = FALSE;
								
								if (token.kind & PPKIND_ELSE)
								{
									lexer->ppCondition[lexer->ppDepth] = !lexer->ppCondition[lexer->ppDepth];
								}
								else
									 lexer->ppCondition[lexer->ppDepth] = TRUE;
								
								if (lexer->ppCondition[lexer->ppDepth])
								{
									if (token.kind & PPKIND_EVALDEF)
									{
										SymbolRef scopeRefs[2];
										
										scopeRefs[0] = lexer->fileScope;
										scopeRefs[1] = EndSymbolRef;

										lexer->ppCondition[lexer->ppDepth] =  NamedDefinition(lexer->sdb, token.text, scopeRefs) != EndSymbolRef;
										if (! lexer->ppCondition[lexer->ppDepth])
										{
											scopeRefs[0] = 0x00000000;
											lexer->ppCondition[lexer->ppDepth] = NamedDefinition(lexer->sdb, token.text, scopeRefs) != EndSymbolRef;
										}

									}
									else if (token.kind & PPKIND_EVALEXPR)
									{
										lexer->ppCondition[lexer->ppDepth] = EvalPPExpression(lexer->sdb, token.text, lexer->fileScope);
									}
									
									if (token.kind & PPKIND_NEGEVAL)
										lexer->ppCondition[lexer->ppDepth] = !lexer->ppCondition[lexer->ppDepth];
								}
								else if (token.kind & PPKIND_ELSE)
									flipBack = TRUE;

								if (lexer->ppCondition[lexer->ppDepth] == FALSE)
								{
									lexer->ppSkipDepth = lexer->ppDepth;
								}
								else
									lexer->ppSkipDepth = 10000;

								if (flipBack)
									lexer->ppCondition[lexer->ppDepth] = TRUE; // just incase there is another elif
							}
							else
								lexer->ppSkipDepth = 10000;
						}
					}
	
					if (lexer->ppDepth >= lexer->ppSkipDepth)
					{
						LexerJumpAfterNextHash(lexer);
						token.kind = TOKEN_HASH;
						ppCmd = lexer->cursor;
					}
					else if (token.kind & PPKIND_CONTROL)
						goto start;						
					else
						return token;
				}
				else if (collectStr)
					lexer->symbolInsertion -= 2;
			}
				
			ignoreNL = (c == '\\');

			c = *lexer->cursor;
		}

		token.kind = TOKEN_END;
	}
	
	if (token.kind == TOKEN_SYMBOL)
	{
		STRPTR pos = lexer->cursor;
		SymbolRef scopeRefs[2];
		SymbolRef defRef;
		
		token.text = lexer->nextFreeSymbol;

		while (lexer->cursor < lexer->contentsEnd
			 && (isalnum(c) || c == '_'))
		{
			token.numChars++;
			lexer->cursor++;
			*lexer->symbolInsertion++ = c;
			c = *lexer->cursor;
		}

		*lexer->symbolInsertion++ = 0;
		
		scopeRefs[0] = lexer->fileScope;
		scopeRefs[1] = EndSymbolRef;
		defRef = NamedDefinition(lexer->sdb, token.text, scopeRefs);
		if (defRef == EndSymbolRef)
		{
			scopeRefs[0] = 0x00000000;
			defRef = NamedDefinition(lexer->sdb, token.text, scopeRefs);
		}

		if (defRef != EndSymbolRef)
		{
			if (LexerResolveDefinition(lexer, pos, defRef))
			{
				lexer->symbolInsertion = lexer->nextFreeSymbol;
				goto start;
			}
		}
				
		lexer->nextFreeSymbol = lexer->symbolInsertion;
		
		token.kind = ValueOfStrKey(lexer->keywordMap, token.text);
		
		if (token.kind == 0) // not found so still a symbol
			token.kind = TOKEN_SYMBOL;
					
		return token;
	}

	if (token.kind == TOKEN_STRING)
	{
		// eat initial "
		lexer->cursor++;
		c = *lexer->cursor;

		while (lexer->cursor < lexer->contentsEnd
			 && (c != '"'))
		{
			if (c == '\\')
				lexer->cursor++;
				
			lexer->cursor++;
			c = *lexer->cursor;
		}
					
		return token;
	}

	if (token.kind == TOKEN_CHARLITERAL)
	{
		// eat initial "
		lexer->cursor++;
		c = *lexer->cursor;

		while (lexer->cursor < lexer->contentsEnd
			 && (c != '\''))
		{
			if (c == '\\')
				lexer->cursor++;
				
			lexer->cursor++;
			c = *lexer->cursor;
		}
					
		return token;
	}

	if (token.kind == TOKEN_NUMBER)
	{
		while (lexer->cursor < lexer->contentsEnd
			 && (isdigit(c) || c == '.'))
		{
			token.numChars++;
			lexer->cursor++;
			c = *lexer->cursor;
		}

		return token;
	}

	token.numChars++;
	lexer->cursor++;

	while (token.kind >= PARTTOKEN_MINUS)
	{
		int followup = FOLLOWUP_INVALID;
		enum TokenKind *tokenOfFollowup;

		c = *lexer->cursor;

		if ((c & 0xFFFFFFE0) == 0x00000020) // range [32;64[
			followup = followupConv[c-32];
		else if (c == '|')
			followup = FOLLOWUP_OR;
					
		tokenOfFollowup = followupToToken[token.kind - PARTTOKEN_MINUS];
		token.kind = tokenOfFollowup[followup];

		if (token.kind != tokenOfFollowup[FOLLOWUP_INVALID])
		{
			token.numChars++;
			lexer->cursor++;
		}
	}

	if (token.kind == TOKEN_COMMENTSTART)
	{
		int previouStar = FALSE;
		
		while (lexer->cursor < lexer->contentsEnd)
		{
			if (previouStar && c == '/')
				goto start;

			previouStar = (c == '*');
				
			if (c == '\n')
				LexerAdvanceBlock(lexer);
			
			c = *(lexer->cursor++);

			token.kind = preDetermine[c];
		}
	}

	if (token.kind == TOKEN_CPPCOMMENT)
	{
		while (lexer->cursor < lexer->contentsEnd)
		{
			lexer->cursor++;
				
			if (c == '\n')
			{
				LexerAdvanceBlock(lexer);
				goto start;
			}
				
			c = *lexer->cursor;
			token.kind = preDetermine[c];
		}
	}

	return token;
}


UWORD preDetermine[256] =
{
    TOKEN_END,     // null
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // newline
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // 
    TOKEN_INVALID, // space
    PARTTOKEN_NOT, // !
    TOKEN_STRING,   // "
    TOKEN_HASH, // #
    TOKEN_INVALID, // dollar
    PARTTOKEN_PERCENT, // %
    PARTTOKEN_AND, // &
    TOKEN_CHARLITERAL, // '
    TOKEN_OPENPAR, // (
    TOKEN_CLOSEPAR, // )
    PARTTOKEN_MULT, // *
    PARTTOKEN_PLUS, // +
    TOKEN_COMMA, // ,
    PARTTOKEN_MINUS, // -
    TOKEN_PERIOD, // .
    PARTTOKEN_DIV, // /
    TOKEN_NUMBER, // 0
    TOKEN_NUMBER, // 1
    TOKEN_NUMBER, // 2
    TOKEN_NUMBER, // 3
    TOKEN_NUMBER, // 4
    TOKEN_NUMBER, // 5
    TOKEN_NUMBER, // 6
    TOKEN_NUMBER, // 7
    TOKEN_NUMBER, // 8
    TOKEN_NUMBER, // 9
    TOKEN_COLON, // :
    TOKEN_SEMICOLON, // ;
    PARTTOKEN_LT, // <
    PARTTOKEN_EQUAL, // =
    PARTTOKEN_GT, // >
    TOKEN_QUESTION, // ?
    TOKEN_INVALID, // at 
    TOKEN_SYMBOL, // A
    TOKEN_SYMBOL, // B
    TOKEN_SYMBOL, // C
    TOKEN_SYMBOL, // D
    TOKEN_SYMBOL, // E
    TOKEN_SYMBOL, // F
    TOKEN_SYMBOL, // G
    TOKEN_SYMBOL, // H
    TOKEN_SYMBOL, // I
    TOKEN_SYMBOL, // J
    TOKEN_SYMBOL, // K
    TOKEN_SYMBOL, // L
    TOKEN_SYMBOL, // M
    TOKEN_SYMBOL, // N
    TOKEN_SYMBOL, // O
    TOKEN_SYMBOL, // P
    TOKEN_SYMBOL, // Q
    TOKEN_SYMBOL, // R
    TOKEN_SYMBOL, // S
    TOKEN_SYMBOL, // T
    TOKEN_SYMBOL, // U
    TOKEN_SYMBOL, // V
    TOKEN_SYMBOL, // W
    TOKEN_SYMBOL, // X
    TOKEN_SYMBOL, // Y
    TOKEN_SYMBOL, // Z
    TOKEN_OPENSQUARE, // [
    TOKEN_BACKSLASH, // backslash
    TOKEN_CLOSESQUARE, // ]
    TOKEN_HAT, // ^
    TOKEN_SYMBOL, // underscore
    TOKEN_INVALID, // accent grave
    TOKEN_SYMBOL, // a
    TOKEN_SYMBOL, // b
    TOKEN_SYMBOL, // c
    TOKEN_SYMBOL, // d
    TOKEN_SYMBOL, // e
    TOKEN_SYMBOL, // f
    TOKEN_SYMBOL, // g
    TOKEN_SYMBOL, // h
    TOKEN_SYMBOL, // i
    TOKEN_SYMBOL, // j
    TOKEN_SYMBOL, // k
    TOKEN_SYMBOL, // l
    TOKEN_SYMBOL, // m
    TOKEN_SYMBOL, // n
    TOKEN_SYMBOL, // o
    TOKEN_SYMBOL, // p
    TOKEN_SYMBOL, // q
    TOKEN_SYMBOL, // r
    TOKEN_SYMBOL, // s
    TOKEN_SYMBOL, // t
    TOKEN_SYMBOL, // u
    TOKEN_SYMBOL, // v
    TOKEN_SYMBOL, // w
    TOKEN_SYMBOL, // x
    TOKEN_SYMBOL, // y
    TOKEN_SYMBOL, // z
    TOKEN_OPENCURLY, // {
    PARTTOKEN_OR, // |
    TOKEN_CLOSECURLY, // }
    TOKEN_TILDE, // ~ 
    TOKEN_INVALID, // DEL
};

UBYTE followupConv[] =
{
    FOLLOWUP_INVALID, // space
    FOLLOWUP_INVALID, // !
    FOLLOWUP_INVALID,   // "
    FOLLOWUP_INVALID, // #
    FOLLOWUP_INVALID, // dollar
    FOLLOWUP_INVALID, // %
    FOLLOWUP_AND, // &
    FOLLOWUP_INVALID, // '
    FOLLOWUP_INVALID, // (
    FOLLOWUP_INVALID, // )
    FOLLOWUP_MULT, // *
    FOLLOWUP_PLUS, // +
    FOLLOWUP_INVALID, // ,
    FOLLOWUP_MINUS, // -
    FOLLOWUP_INVALID, // .
    FOLLOWUP_DIV, // /
    FOLLOWUP_INVALID, // 0
    FOLLOWUP_INVALID, // 1
    FOLLOWUP_INVALID, // 2
    FOLLOWUP_INVALID, // 3
    FOLLOWUP_INVALID, // 4
    FOLLOWUP_INVALID, // 5
    FOLLOWUP_INVALID, // 6
    FOLLOWUP_INVALID, // 7
    FOLLOWUP_INVALID, // 8
    FOLLOWUP_INVALID, // 9
    FOLLOWUP_INVALID, // :
    FOLLOWUP_INVALID, // ;
    FOLLOWUP_LT, // <
    FOLLOWUP_EQUAL, // =
    FOLLOWUP_GT, // >
    FOLLOWUP_INVALID, // ?
};

enum TokenKind followupToToken[][10] =
{
	{ // PARTTOKEN_MINUS
		TOKEN_MINUS, // FOLLOWUP_INVALID,
		TOKEN_MINUS, // FOLLOWUP_PLUS,
		PARTTOKEN_MINUSMINUS, // FOLLOWUP_MINUS,
		TOKEN_PTRINDIRECT, // FOLLOWUP_GT,
		TOKEN_MINUS, // FOLLOWUP_LT,
		TOKEN_MINUSEQUAL, // FOLLOWUP_EQUAL
		TOKEN_MINUS, // FOLLOWUP_AND
		TOKEN_MINUS, // FOLLOWUP_OR
		TOKEN_MINUS, // FOLLOWUP_MULT
		TOKEN_MINUS // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_PLUS
		TOKEN_PLUS, // FOLLOWUP_INVALID,
		PARTTOKEN_PLUSPLUS, // FOLLOWUP_PLUS,
		TOKEN_MINUS, // FOLLOWUP_MINUS,
		TOKEN_PLUS, // FOLLOWUP_GT,
		TOKEN_PLUS, // FOLLOWUP_LT,
		TOKEN_PLUSEQUAL, // FOLLOWUP_EQUAL
		TOKEN_PLUS, // FOLLOWUP_AND
		TOKEN_PLUS, // FOLLOWUP_OR
		TOKEN_PLUS, // FOLLOWUP_MULT
		TOKEN_PLUS // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_MULT
		TOKEN_MULT, // FOLLOWUP_INVALID,
		TOKEN_MULT, // FOLLOWUP_PLUS,
		TOKEN_MULT, // FOLLOWUP_MINUS,
		TOKEN_MULT, // FOLLOWUP_GT,
		TOKEN_MULT, // FOLLOWUP_LT,
		TOKEN_MULTEQUAL, // FOLLOWUP_EQUAL
		TOKEN_MULT, // FOLLOWUP_AND
		TOKEN_MULT, // FOLLOWUP_OR
		TOKEN_MULT, // FOLLOWUP_MULT
		TOKEN_MULT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_DIV
		TOKEN_DIV, // FOLLOWUP_INVALID,
		TOKEN_DIV, // FOLLOWUP_PLUS,
		TOKEN_DIV, // FOLLOWUP_MINUS,
		TOKEN_DIV, // FOLLOWUP_GT,
		TOKEN_DIV, // FOLLOWUP_LT,
		TOKEN_DIVEQUAL, // FOLLOWUP_EQUAL
		TOKEN_DIV, // FOLLOWUP_AND
		TOKEN_DIV, // FOLLOWUP_OR
		TOKEN_COMMENTSTART, // FOLLOWUP_MULT
		TOKEN_CPPCOMMENT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_EQUAL
		TOKEN_EQUAL, // FOLLOWUP_INVALID,
		TOKEN_EQUAL, // FOLLOWUP_PLUS,
		TOKEN_EQUAL, // FOLLOWUP_MINUS,
		TOKEN_EQUAL, // FOLLOWUP_GT,
		TOKEN_EQUAL, // FOLLOWUP_LT,
		TOKEN_EQUALEQUAL, // FOLLOWUP_EQUAL
		TOKEN_EQUAL, // FOLLOWUP_AND
		TOKEN_EQUAL, // FOLLOWUP_OR
		TOKEN_EQUAL, // FOLLOWUP_MULT
		TOKEN_EQUAL // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_LT
		TOKEN_LT, // FOLLOWUP_INVALID,
		TOKEN_LT, // FOLLOWUP_PLUS,
		TOKEN_LT, // FOLLOWUP_MINUS,
		TOKEN_LT, // FOLLOWUP_GT,
		TOKEN_LT, // FOLLOWUP_LT,
		TOKEN_LESSOREQUAL, // FOLLOWUP_EQUAL
		TOKEN_LT, // FOLLOWUP_AND
		TOKEN_LT, // FOLLOWUP_OR
		TOKEN_LT, // FOLLOWUP_MULT
		TOKEN_LT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_GT
		TOKEN_GT, // FOLLOWUP_INVALID,
		TOKEN_GT, // FOLLOWUP_PLUS,
		TOKEN_GT, // FOLLOWUP_MINUS,
		TOKEN_GT, // FOLLOWUP_GT,
		TOKEN_GT, // FOLLOWUP_LT,
		TOKEN_GREATEROREQUAL, // FOLLOWUP_EQUAL
		TOKEN_GT, // FOLLOWUP_AND
		TOKEN_GT, // FOLLOWUP_OR
		TOKEN_GT, // FOLLOWUP_MULT
		TOKEN_GT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_NOT
		TOKEN_NOT, // FOLLOWUP_INVALID,
		TOKEN_NOT, // FOLLOWUP_PLUS,
		TOKEN_NOT, // FOLLOWUP_MINUS,
		TOKEN_NOT, // FOLLOWUP_GT,
		TOKEN_NOT, // FOLLOWUP_LT,
		TOKEN_NOTEQUAL, // FOLLOWUP_EQUAL
		TOKEN_NOT, // FOLLOWUP_AND
		TOKEN_NOT, // FOLLOWUP_OR
		TOKEN_NOT, // FOLLOWUP_MULT
		TOKEN_NOT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_PERCENT
		TOKEN_PERCENT, // FOLLOWUP_INVALID,
		TOKEN_PERCENT, // FOLLOWUP_PLUS,
		TOKEN_PERCENT, // FOLLOWUP_MINUS,
		TOKEN_PERCENT, // FOLLOWUP_GT,
		TOKEN_PERCENT, // FOLLOWUP_LT,
		TOKEN_PERCENTEQUAL, // FOLLOWUP_EQUAL
		TOKEN_PERCENT, // FOLLOWUP_AND
		TOKEN_PERCENT, // FOLLOWUP_OR
		TOKEN_PERCENT, // FOLLOWUP_MULT
		TOKEN_PERCENT // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_OR
		TOKEN_OR, // FOLLOWUP_INVALID,
		TOKEN_OR, // FOLLOWUP_PLUS,
		TOKEN_OR, // FOLLOWUP_MINUS,
		TOKEN_OR, // FOLLOWUP_GT,
		TOKEN_OR, // FOLLOWUP_LT,
		TOKEN_OR, // FOLLOWUP_EQUAL
		TOKEN_OR, // FOLLOWUP_AND
		TOKEN_LOGICAL_OR, // FOLLOWUP_OR
		TOKEN_OR, // FOLLOWUP_MULT
		TOKEN_OR // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_AND
		TOKEN_AND, // FOLLOWUP_INVALID,
		TOKEN_AND, // FOLLOWUP_PLUS,
		TOKEN_AND, // FOLLOWUP_MINUS,
		TOKEN_AND, // FOLLOWUP_GT,
		TOKEN_AND, // FOLLOWUP_LT,
		TOKEN_AND, // FOLLOWUP_EQUAL
		TOKEN_LOGICAL_AND, // FOLLOWUP_AND
		TOKEN_AND, // FOLLOWUP_OR
		TOKEN_AND, // FOLLOWUP_MULT
		TOKEN_AND // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_MINUSMINUS
		TOKEN_MINUSMINUS, // FOLLOWUP_INVALID,
		TOKEN_MINUSMINUS, // FOLLOWUP_PLUS,
		TOKEN_MINUSMINUS, // FOLLOWUP_MINUS,
		TOKEN_MINUSMINUS, // FOLLOWUP_GT,
		TOKEN_MINUSMINUS, // FOLLOWUP_LT,
		TOKEN_MINUSMINUSEQUAL, // FOLLOWUP_EQUAL
		TOKEN_MINUSMINUS, // FOLLOWUP_AND
		TOKEN_MINUSMINUS, // FOLLOWUP_OR
		TOKEN_MINUSMINUS, // FOLLOWUP_MULT
		TOKEN_MINUSMINUS // FOLLOWUP_DIV
	},
	{ // PARTTOKEN_PLUSPLUS
		TOKEN_PLUSPLUS, // FOLLOWUP_INVALID,
		TOKEN_PLUSPLUS, // FOLLOWUP_PLUS,
		TOKEN_PLUSPLUS, // FOLLOWUP_MINUS,
		TOKEN_PLUSPLUS, // FOLLOWUP_GT,
		TOKEN_PLUSPLUS, // FOLLOWUP_LT,
		TOKEN_PLUSPLUSEQUAL, // FOLLOWUP_EQUAL
		TOKEN_PLUSPLUS, // FOLLOWUP_AND
		TOKEN_PLUSPLUS, // FOLLOWUP_OR
		TOKEN_PLUSPLUS, // FOLLOWUP_MULT
		TOKEN_PLUSPLUS // FOLLOWUP_DIV
	}
};
