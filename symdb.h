#ifndef SDB_H
#define SDB_H

#include <exec/types.h>
#include <dos/dos.h>

typedef ULONG SymbolRef;

#define SDB_INSTANCE 0
#define SDB_MEMBER 1
#define SDB_SCOPE 2
#define SDB_COMPOUND 3
#define SDB_SCOPEEND 4
#define SDB_TYPE 5
#define SDB_DEFINITION 6
#define SDB_OBSOLETE 7

#define EndSymbolRef (~0)

#define NoFileRef (0xFFFF)


UWORD FindFileID(struct SymDb *sdb, STRPTR filePath);

SymbolRef StartCodeFile(APTR sdb, STRPTR filePath, ULONG fileSize, struct DateStamp *fileTimeStamp, ULONG markAsStatic, ULONG fileID);
UWORD FinalizeCodeFile(APTR sdb, SymbolRef startScope, ULONG blockNum);

BOOL IsObsolete(struct SymDb *sdb, ULONG fileID, struct FileInfoBlock *fib, ULONG skipStatic);

SymbolRef AddSymbol(APTR sdb, STRPTR name, SymbolRef partOf, SymbolRef typeId, ULONG kind, ULONG blockNum);
void AddInclude(struct SymDb *sdb, SymbolRef currentFileScope, UWORD includeFileRef);
void StoreDefinitionValue(APTR sdb, SymbolRef ref, STRPTR value);
void StoreDefinitionArgs(APTR sdb, SymbolRef ref, STRPTR args);
STRPTR GetDefinitionValue(APTR sdb, SymbolRef ref);
STRPTR GetDefinitionArgs(APTR sdb, SymbolRef ref);

void ChangeSymbolKind(APTR sdb, SymbolRef symRef, ULONG newKind);
void DeleteTrailingSymbols(APTR sdb, SymbolRef lastKeptSymRef);

SymbolRef NamedCompound(APTR sdb, STRPTR name, SymbolRef *scopeRefs);
SymbolRef NamedKind(APTR sdb, STRPTR name, UBYTE kind, SymbolRef *scopeRefs);
#define NamedType(sdb, name, scopeRefs) NamedKind(sdb, name, SDB_TYPE, scopeRefs)
#define NamedDefinition(sdb, name, scopeRefs) NamedKind(sdb, name, SDB_DEFINITION, scopeRefs)

#endif // SDB_H