VERSION		EQU	1
REVISION	EQU	1

DATE	MACRO
		dc.b '3.2.2024'
		ENDM

VERS	MACRO
		dc.b 'codeindex.library 1.1'
		ENDM

VSTRING	MACRO
		dc.b 'codeindex.library 1.1 (3.2.2024)',13,10,0
		ENDM

VERSTAG	MACRO
		dc.b 0,'$VER: codeindex.library 1.1 (3.2.2024)',0
		ENDM
