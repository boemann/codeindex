#include <exec/types.h>


#include <proto/exec.h>
#include <proto/dos.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <containerkit/map.h>
#include <clib/containerkit_protos.h>
#include <pragmas/containerkit_pragmas.h>

#include "symdb.h"
#include "lvo_clib.h"

#include "lexer_C.h"

/* The PPEvaluator is both a mini lexer and a parser in the same struct
 * but it is still split conceptually
 */
struct PPEvaluator
{
	Array textArray;
	STRPTR contents;
	STRPTR contentsEnd;
	STRPTR cursor;
	APTR sdb;
	SymbolRef fileScope;
	BOOL CheckDefMode;
	struct Token token;
};

 
// these 3 are defined in lexer_C.c
extern UWORD preDetermine[256];
extern UBYTE followupConv[];
extern enum TokenKind followupToToken[][10];


extern struct Library *ContainerkitBase;

void SetupPPExprLexer(struct PPEvaluator *ppEval, APTR sdb, STRPTR contents, SymbolRef fileScope)
{
	ppEval->sdb = sdb;
	ppEval->fileScope = fileScope;
	ppEval->textArray = NewArray(0);
	ResizeArray(ppEval->textArray, strlen(contents)+1);

	ppEval->contents = ArrayValues(UBYTE, ppEval->textArray);

	strcpy(ppEval->contents, contents);

	ppEval->contentsEnd = ppEval->contents + SizeOfArray(ppEval->textArray) - 1;
	ppEval->cursor = ppEval->contents;
	ppEval->CheckDefMode = FALSE;
}

static void injectDefineValue(struct PPEvaluator *ppEval, STRPTR pos, STRPTR newText)
{
	int index = pos - ppEval->contents;
	int oldLength = ppEval->cursor - pos;
	int newLength = strlen(newText);
	
	if (oldLength < newLength)
		InsertArrayElements(ppEval->textArray, index, newLength - oldLength);
	else
		EraseArrayElements(ppEval->textArray, index, oldLength - newLength);
		
	// addresss might've changed
	ppEval->contents = ArrayValues(UBYTE, ppEval->textArray);
	ppEval->contentsEnd = ppEval->contents + SizeOfArray(ppEval->textArray) - 1;
	
	CopyMem(newText, ppEval->contents + index, newLength);
	
	ppEval->cursor = ppEval->contents + index;
}

static struct Token nextToken(struct PPEvaluator *ppEval)
{
	struct Token token;
	char c;	

start:

	c = *ppEval->cursor;

	token.kind = preDetermine[c];

	// let's strip off spacing and invalids
	while (ppEval->cursor < ppEval->contentsEnd
			&& (token.kind == TOKEN_INVALID))
	{
		ppEval->cursor++;

		c = *ppEval->cursor;
		token.kind = preDetermine[c];
	}

	
	token.numChars = 0;
	
	if (ppEval->cursor >= ppEval->contentsEnd)
	{
		token.kind = TOKEN_END;
		return token;
	}
		
	// from above predetermine we already know what token kind to expect

	if (token.kind == TOKEN_SYMBOL)
	{
		TEXT sym[256];
		STRPTR symP = sym;
		STRPTR pos = ppEval->cursor;
		STRPTR newText;
		SymbolRef scopeRefs[2];
		SymbolRef defRef;

		token.text  = pos; // hack for Symbol "defined" case
		
		while (ppEval->cursor < ppEval->contentsEnd
			 && (isalnum(c) || c == '_'))
		{
			token.numChars++;
			ppEval->cursor++;
			*symP++ = c;
			c = *ppEval->cursor;
		}

		*symP++ = 0;
		
		scopeRefs[0] = ppEval->fileScope;
		scopeRefs[1] = EndSymbolRef;
		defRef = NamedDefinition(ppEval->sdb, sym, scopeRefs);
		if (defRef == EndSymbolRef)
		{
			scopeRefs[0] = 0x00000000;
			defRef = NamedDefinition(ppEval->sdb, sym, scopeRefs);
		}

		if (ppEval->CheckDefMode)
		{
			token.text = (STRPTR)(defRef != EndSymbolRef);
			return token;
		}
		
		newText = NULL;
		
		if (defRef != EndSymbolRef)
			newText = GetDefinitionValue(ppEval->sdb, defRef);
		
		if (!newText)
			return token;
			
		injectDefineValue(ppEval, pos, newText);
		goto start;
	}

	if (token.kind == TOKEN_STRING)
	{
		token.kind = TOKEN_INVALID;
		return token;
	}

	if (token.kind == TOKEN_CHARLITERAL)
	{
		// eat initial "
		ppEval->cursor++;
		c = *ppEval->cursor;

		token.text = (STRPTR)c;
		
		while (ppEval->cursor < ppEval->contentsEnd
			 && (c != '\''))
		{
			if (c == '\\')
				ppEval->cursor++;
				
			ppEval->cursor++;
			c = *ppEval->cursor;
		}
					
		return token;
	}

	if (token.kind == TOKEN_NUMBER)
	{
		LONG val = 0;
		
		while (ppEval->cursor < ppEval->contentsEnd
			 && (isdigit(c)))
		{
			val *= 10;
			val += c - '0';
			token.numChars++;
			ppEval->cursor++;
			c = *ppEval->cursor;
		}
		token.text = (STRPTR)val;
		return token;
	}

	token.numChars++;
	ppEval->cursor++;

	while (token.kind >= PARTTOKEN_MINUS)
	{
		int followup = FOLLOWUP_INVALID;
		enum TokenKind *tokenOfFollowup;

		c = *ppEval->cursor;

		if ((c & 0xFFFFFFE0) == 0x00000020) // range [32;64[
			followup = followupConv[c-32];
		else if (c == '|')
			followup = FOLLOWUP_OR;
					
		tokenOfFollowup = followupToToken[token.kind - PARTTOKEN_MINUS];
		token.kind = tokenOfFollowup[followup];

		if (token.kind != tokenOfFollowup[FOLLOWUP_INVALID])
		{
			token.numChars++;
			ppEval->cursor++;
		}
	}

	if (token.kind == TOKEN_COMMENTSTART)
	{
		int previouStar = FALSE;
		
		while (ppEval->cursor < ppEval->contentsEnd)
		{
			ppEval->cursor++;

			if (previouStar && c == '/')
				goto start;

			previouStar = (c == '*');
							
			c = *ppEval->cursor;
			token.kind = preDetermine[c];
		}
	}

	if (token.kind == TOKEN_CPPCOMMENT)
		token.kind = TOKEN_END;

	return token;
}

static void PPEvaluatorEatToken(struct PPEvaluator *ppEval)
{
	ppEval->token = nextToken(ppEval);
}

int ppOrExpr(struct PPEvaluator *ppEval);

int ppFactorExpr(struct PPEvaluator *ppEval)
{
	int retval = 0;
	if (ppEval->token.kind == TOKEN_OPENPAR)
	{
		PPEvaluatorEatToken(ppEval);
		retval = ppOrExpr(ppEval);
		PPEvaluatorEatToken(ppEval); // CLOSEPAR
		return retval;
	}
	if (ppEval->token.kind == TOKEN_NUMBER)
		retval = (LONG) ppEval->token.text;
	if (ppEval->token.kind == TOKEN_SYMBOL)
	{
		if (strncmp(ppEval->token.text, "defined", 7) == 0)
		{
			ppEval->CheckDefMode = 1;
			PPEvaluatorEatToken(ppEval); // "defined"
			if (ppEval->token.kind == TOKEN_OPENPAR)
				PPEvaluatorEatToken(ppEval);
				
			retval = 0;
			ppEval->CheckDefMode = 0; // The job has been done
			
			if (ppEval->token.kind == TOKEN_SYMBOL)
			{
				retval = (int)ppEval->token.text; // bool when in CheckDefMode
				PPEvaluatorEatToken(ppEval); // definition val
			}
				
			if (ppEval->token.kind == TOKEN_CLOSEPAR)
				PPEvaluatorEatToken(ppEval);
			return retval;
		}
	}
	PPEvaluatorEatToken(ppEval);
	return retval;		
}

int ppUnaryExpr(struct PPEvaluator *ppEval)
{
	int negate = FALSE;
	while (ppEval->token.kind == TOKEN_MINUS)
	{
		PPEvaluatorEatToken(ppEval);
		negate = !negate;
	}
	if (negate)
		return - ppFactorExpr(ppEval);
	return ppFactorExpr(ppEval);
}

int ppMulExpr(struct PPEvaluator *ppEval)
{
	int retval = ppUnaryExpr(ppEval);
	enum TokenKind kind = ppEval->token.kind;

	while (kind == TOKEN_MULT || kind == TOKEN_DIV)
	{
		PPEvaluatorEatToken(ppEval);
		if (kind == TOKEN_MULT)
			retval *= ppUnaryExpr(ppEval);
		else
		{
			int tval = ppUnaryExpr(ppEval);
			if (tval)
				retval /= tval;
		}
		kind = ppEval->token.kind;
	}
	return retval;
}

int ppSumExpr(struct PPEvaluator *ppEval)
{
	int retval = ppMulExpr(ppEval);
	enum TokenKind kind = ppEval->token.kind;

	while (kind == TOKEN_PLUS || kind == TOKEN_MINUS)
	{
		PPEvaluatorEatToken(ppEval);
		if (kind == TOKEN_PLUS)
			retval += ppMulExpr(ppEval);
		else
			retval -= ppMulExpr(ppEval);
		kind = ppEval->token.kind;
	}
	return retval;
}

int ppRelExpr(struct PPEvaluator *ppEval)
{
	int retval = ppSumExpr(ppEval);
	enum TokenKind kind = ppEval->token.kind;
	
	if (kind < TOKEN_LT || kind > TOKEN_NOTEQUAL)
		return retval;

	PPEvaluatorEatToken(ppEval);
		
	if (kind == TOKEN_EQUALEQUAL)
		return retval == ppSumExpr(ppEval);
	
	if (kind == TOKEN_LT)
		return retval < ppSumExpr(ppEval);
		
	if (kind == TOKEN_GT)
		return retval > ppSumExpr(ppEval);
		
	if (kind == TOKEN_LESSOREQUAL)
		return retval <= ppSumExpr(ppEval);
		
	if (kind == TOKEN_GREATEROREQUAL)
		return retval >= ppSumExpr(ppEval);
		
	if (kind == TOKEN_NOTEQUAL)
		return retval != ppSumExpr(ppEval);
		
	return retval;
}


int ppUnaryRelExpr(struct PPEvaluator *ppEval)
{
	int negate = FALSE;
	while (ppEval->token.kind == TOKEN_NOT)
	{
		PPEvaluatorEatToken(ppEval);
		negate = !negate;
	}
	if (negate)
		return ! ppRelExpr(ppEval);
	return ppRelExpr(ppEval);
}

int ppAndExpr(struct PPEvaluator *ppEval)
{
	int retval = ppUnaryRelExpr(ppEval);
	while (ppEval->token.kind == TOKEN_LOGICAL_AND)
	{
		PPEvaluatorEatToken(ppEval);
		retval = ppUnaryRelExpr(ppEval) && retval;
	}
	return retval;
}

int ppOrExpr(struct PPEvaluator *ppEval)
{
	int retval = ppAndExpr(ppEval);
	while (ppEval->token.kind == TOKEN_LOGICAL_OR)
	{
		PPEvaluatorEatToken(ppEval);
		retval = ppAndExpr(ppEval) || retval;
	}
	return retval;
}

int EvalPPExpression(APTR sdb, STRPTR text, SymbolRef fileScope)
{
	struct PPEvaluator ppEval;
	
	SetupPPExprLexer(&ppEval, sdb, text, fileScope);
		
	PPEvaluatorEatToken(&ppEval);

	return ppOrExpr(&ppEval);
}