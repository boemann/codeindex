#define VERSION		1
#define REVISION	1
#define DATE		"3.2.2024"
#define VERS		"codeindex.library 1.1"
#define VSTRING		"codeindex.library 1.1 (3.2.2024)\r\n"
#define VERSTAG		"\0$VER: codeindex.library 1.1 (3.2.2024)"
