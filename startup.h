/*
 * :ts=4
 */

#ifndef STARTUP_H
#define STARTUP_H

/*
Copyright (C) 2021 Camilla Boemann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 2.1 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/****************************************************************************/

#ifndef PROTO_EXEC_H
#define __USE_SYSBASE
#include <proto/exec.h>
#endif /* PROTO_EXEC_H */

#ifndef PROTO_DOS_H
#include <proto/dos.h>
#endif /* PROTO_DOS_H */

/****************************************************************************/

struct CodeIndexBase
{
	struct Library			cb_Library;
	BPTR					cb_LibrarySegment;
	struct SignalSemaphore	cb_LockSemaphore;
};

/****************************************************************************/

#endif /* STARTUP_H */
