#define __CLIB_PRAGMA_LIBCALL
#include <proto/exec.h>
#include <proto/dos.h>

#include <containerkit/map.h>
#include <clib/containerkit_protos.h>
#include <pragmas/containerkit_pragmas.h>

#include <testkit/test.h>

extern struct Library *ContainerkitBase;

#include <stdio.h>
#include <string.h>

#include "symdb_p.h"
#include "symdb.h"
#include "lvo_clib.h"
#define	__NOLIBBASE__

char *stacksize = "$STACK:8192"; // only works when started from CLI


TEXT hFileText[] =
"struct stype\n\
{\n\
	int a;\n\
	char amiga;\n\
	float macintosh;\n\
};\n\
\n\
int glob2;\n\
struct stype globs;\n\
#define USIGN unsigned\n\
#define MYT int\n\
#define MYST struct stype\n\
typedef int myint;\n\
typedef MYT myuint;\n\
typedef MYST myst;\n\
typedef USIGN int myuint;\n\
typedef const USIGN int mycuint;\n\
";

TEXT c1FileText[] =
"#include \"ram:indextest/.h\"\n\
\n\
int foobar(int xxx)\n\
{\n\
	int fval;\n\
	{\n\
		struct stype inst;\n\
		int fval2;\n\
	}\n\
}\n\
int glob;\n\
";

TEXT c2FileText[] =
"\n\
#define YES 5\n\
#ifdef YES\n\
int F;\n\
#ifdef NO\n\
int B;\n\
#elif 1\n\
int i;\n\
#else\n\
int a;\n\
#endif\n\
int n;\n\
#endif\n\
\n\
\n\
#if 0 + 25 -1 == (6-2 * 8)\n\
int d;\n\
#else\n\
int e;\n\
#endif\n\
\n\
\n\
#ifdef NOTDEF\n\
#ifdef NO\n\
#else\n\
int oops;\n\
#endif\n\
#endif\n\
\n\
#if _AMIGA\n\
int YESAmiga;\n\
#endif\n\
";

STRPTR searchPaths[] = 
{
	//"PROJECTDIR:projectdir",
	"NDK:include_h",
	NULL
};

APTR createFixture(void)
{
	struct SymDb *sdb = (struct SymDb *)CreateSymDb(0);	
	BPTR file;
	struct ParseAuxInfo pai;
	
	System("delete ram:indextest all", NULL);
	System("makedir ram:indextest", NULL);
	
	file = Open("ram:indextest/.h", MODE_NEWFILE);
	Write(file, hFileText, strlen(hFileText));
	Close (file);

	file = Open("ram:indextest/1.c", MODE_NEWFILE);
	Write(file, c1FileText, strlen(c1FileText));
	Close (file);

	file = Open("ram:indextest/2.c", MODE_NEWFILE);
	Write(file, c2FileText, strlen(c2FileText));
	Close (file);

	CreateCodeIndexProject("ram:indextest");
	
	pai.searchPaths = searchPaths;
	
	ParseFile(sdb, "ram:indextest/1.c", NULL, &pai);
	ParseFile(sdb, "ram:indextest/2.c", NULL, &pai);

	return sdb;
}

void DestroyFixture(APTR sdb)
{
	DestroySymDb(sdb);
}

TESTKIT_SUITE(CodeIndex)

TESTKIT_TEST(SimpleLookup)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();
	
	completionCmd = GetCompletionCommand(sdb, NULL, "gl", "ram:indextest/.h", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates glob2 globs");
		FreeVec(completionCmd);
	}
	DestroyFixture(sdb);
}
		
TESTKIT_TEST(GlobalLookup)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();
	
	completionCmd = GetCompletionCommand(sdb, NULL, "gl", "ram:indextest/1.c", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates glob glob2 globs");
		FreeVec(completionCmd);
	}
	DestroyFixture(sdb);
}

TESTKIT_TEST(testMemberLookup)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();
	
	completionCmd = GetCompletionCommand(sdb, "inst", "m", "ram:indextest/1.c", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates macintosh");
		FreeVec(completionCmd);
	}		
	
	completionCmd = GetCompletionCommand(sdb, "globs", "a", "ram:indextest/1.c", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates a amiga");
		FreeVec(completionCmd);
	}
	DestroyFixture(sdb);
}

TESTKIT_TEST(FailingLookup)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();
	
	completionCmd = GetCompletionCommand(sdb, "inst", "d", "ram:indextest/1.c", 8);
	CHECK(!completionCmd);
	DestroyFixture(sdb);
}

TESTKIT_TEST(ScopeLimiting)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();
	
	completionCmd = GetCompletionCommand(sdb, NULL, "f", "ram:indextest/1.c", 1);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates foobar");
		FreeVec(completionCmd);
	}

	completionCmd = GetCompletionCommand(sdb, NULL, "f", "ram:indextest/1.c", 4);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates foobar fval");
		FreeVec(completionCmd);
	}

	completionCmd = GetCompletionCommand(sdb, NULL, "f", "ram:indextest/1.c", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates foobar fval fval2");
		FreeVec(completionCmd);
	}

	completionCmd = GetCompletionCommand(sdb, NULL, "f", "ram:indextest/1.c",11);
	CHECK(completionCmd);

    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates foobar");
		FreeVec(completionCmd);
	}
	DestroyFixture(sdb);
}

TESTKIT_TEST(Preproccesor)
{
	STRPTR completionCmd;
	APTR sdb = createFixture();

	completionCmd = GetCompletionCommand(sdb, NULL, "", "ram:indextest/2.c", 8);
	CHECK(completionCmd);
    if (completionCmd)
    {
		CHECK_STR_EQ(completionCmd, "COMPLETE candidates YES F i n e YESAmiga");
		FreeVec(completionCmd);
	}

	DestroyFixture(sdb);
}

TESTKIT_SUITE_END

int main(int argc, char **argv)
{
	TESTKIT_RUN_SUITE(CodeIndex);
	return  1;
}

