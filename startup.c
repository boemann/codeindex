/*
 * :ts=4
 */
/*
Copyright (C) 2021 Camilla Boemann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, version 2.1 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <exec/resident.h>

/****************************************************************************/

#include "startup.h"
#include "symdb.h"
#include "lvo_clib.h"

/****************************************************************************/

#include "codeindex.library_rev.h"

/****************************************************************************/

LONG
ReturnError(void)
{
	return -1;
}

void _XCEXIT()
{
}

/****************************************************************************/

struct ExecBase *SysBase;
struct Library *UtilityBase;
struct DosLibrary *DOSBase;

static struct CodeIndexBase * __ASM__
LibInit(
	__REG__(d0, struct CodeIndexBase *	cb),
	__REG__(a0, BPTR						librarySegment),
	__REG__(a6, struct Library *			sBase))
{
	struct CodeIndexBase * return_value = NULL;

	if (sBase->lib_Version >= 39)
	{
		SysBase = (struct ExecBase *)sBase;

		cb->cb_LibrarySegment = librarySegment;

		cb->cb_Library.lib_Revision = REVISION;

		InitSemaphore(&cb->cb_LockSemaphore);
		
		return_value = cb;
	}

	if (return_value == NULL)
	{
		FreeMem((BYTE *)cb - cb->cb_Library.lib_NegSize,
			cb->cb_Library.lib_NegSize + cb->cb_Library.lib_PosSize);
	}

	return return_value;
}

/****************************************************************************/

static struct CodeIndexBase * __ASM__
LibOpen(__REG__(a6, struct CodeIndexBase *cb))
{
	struct CodeIndexBase * return_value = cb;
	BOOL is_first_opener;

	is_first_opener = (BOOL)(cb->cb_Library.lib_OpenCnt == 0);

	cb->cb_Library.lib_OpenCnt++;
	cb->cb_Library.lib_Flags &= ~LIBF_DELEXP;

	ObtainSemaphore(&cb->cb_LockSemaphore);

	/* Perform initial library initialization here, e.g. open disk-based
	 * libraries which require that the caller is a Process and not a
	 * plain Task.
	 */
	if (is_first_opener)
	{
		/* If opening failed, set return_value = NULL. */
		UtilityBase = OpenLibrary("utility.library", 47);
		DOSBase = (struct DosLibrary *)OpenLibrary("dos.library", 47);

	}

	if (return_value == NULL)
	{
		/* Perform any cleanup work here if opening failed. */

		cb->cb_Library.lib_OpenCnt--;
	}

	ReleaseSemaphore(&cb->cb_LockSemaphore);

	return return_value;
}

/****************************************************************************/

static BPTR __ASM__
LibExpunge(__REG__(a6, struct CodeIndexBase *cb))
{
	BPTR return_value = (BPTR)NULL;

	if (cb->cb_Library.lib_OpenCnt == 0)
	{
		return_value = cb->cb_LibrarySegment;

		Remove((struct Node *)cb);

		FreeMem((BYTE *)cb - cb->cb_Library.lib_NegSize,
			cb->cb_Library.lib_NegSize + cb->cb_Library.lib_PosSize);
	}
	else
	{
		cb->cb_Library.lib_Flags |= LIBF_DELEXP;
	}

	return return_value;
}

/****************************************************************************/

static BPTR __ASM__
LibClose(__REG__(a6, struct CodeIndexBase *cb))
{
	BPTR return_value = NULL;

	ObtainSemaphore(&cb->cb_LockSemaphore);

	/* Perform final library cleanup. Note that the library open count
	 * remains > 0 to avoid LibExpunge() freeing the library base, etc.
	 */
	if (cb->cb_Library.lib_OpenCnt == 1)
	{
		CloseLibrary(UtilityBase);
		UtilityBase = NULL;
		CloseLibrary((struct Library *)DOSBase);
		DOSBase = NULL;
	}

	cb->cb_Library.lib_OpenCnt--;

	ReleaseSemaphore(&cb->cb_LockSemaphore);

	if (cb->cb_Library.lib_OpenCnt == 0 && (cb->cb_Library.lib_Flags & LIBF_DELEXP))
		return_value = LibExpunge(cb);

	return return_value;
}

/****************************************************************************/

static LONG
LibReserved(void)
{
	return 0;
}

/****************************************************************************/

static const APTR LibVectors[] =
{
	LibOpen,
	LibClose,
	LibExpunge,
	LibReserved,

	CreateSymDb,
	DestroySymDb,

	CreateCodeIndexProject,
	ParseFile,

	GetCompletionCommand,

	(APTR) -1
};

/****************************************************************************/

struct LibraryInitTable
{
	ULONG	lit_BaseSize;
	APTR *	lit_VectorTable;
	APTR	lit_InitTable;
	APTR	lit_InitRoutine;
};

static struct LibraryInitTable LibInitTable =
{
	sizeof(struct CodeIndexBase),
	LibVectors,
	NULL,
	(APTR)LibInit
};

/****************************************************************************/

const struct Resident RomTag =
{
	RTC_MATCHWORD,
	(struct Resident *)&RomTag,
	(struct Resident *)&RomTag+1,	/* Right behind the RomTag */
	RTF_AUTOINIT,
	VERSION,
	NT_LIBRARY,
	0,
	"codeindex.library",
	VSTRING,
	&LibInitTable
};
