#include <exec/types.h>
#include "symdb.h"
#include "lvo_clib.h"
#include "symdb_p.h"

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/dos.h>

#include <containerkit/map.h>
#include <clib/containerkit_protos.h>
#include <pragmas/containerkit_pragmas.h>

struct Library *ContainerkitBase;

#include <string.h>
#include <stdio.h>

BPTR LockCodeIndexDirOfFileID(UWORD fileID);

static void destroyFileSymbols(struct FileSymbols *fs)
{
	if (! fs)
		return;

	if (fs->symbols)
		FreeVec(fs->symbols);

	if (fs->includesArray)
		DeleteArray(fs->includesArray);
	else if (fs->includes)
		FreeVec(fs->includes);

	if (fs->scopeStartsArray)
		DeleteArray(fs->scopeStartsArray);
	else if (fs->scopeStarts)
		FreeVec(fs->scopeStarts);

	if (fs->sortedScopeEndsArray)
		DeleteArray(fs->sortedScopeEndsArray);
	else if (fs->sortedScopeEnds)
		FreeVec(fs->sortedScopeEnds);

	if (fs->stringsArray)
		DeleteArray(fs->stringsArray);
	else if (fs->strings)
		FreeVec(fs->strings);
		
	if (fs->filePath)
		FreeVec(fs->filePath);
		
	if (fs->definitionValues)
		DeleteMap(fs->definitionValues);

	if (fs->definitionArgs)
		DeleteMap(fs->definitionArgs);

	if (fs->nameMap)		
		DeleteMap(fs->nameMap);
	
	FreeMem(fs, sizeof(struct FileSymbols));
}

static void saveFileSymbols(struct FileSymbols *fs)
{
	int i = 0;
	int stringOffset;
	struct Symbol *symbol = fs->symbols;
	BPTR fh;
	BPTR oldDir;
	TEXT dbFileName[8];
	int nameLen;
	
	oldDir = CurrentDir(fs->codeIndexDirLock);
	
	SNPrintf(dbFileName, 8, "%04lx.db", fs->fileID);
	fh = Open(dbFileName, MODE_NEWFILE);
	CurrentDir(oldDir);

	if (! fh)
		return;
		
	FWrite(fh, &fs->fileSize, sizeof(fs->fileSize), 1);
	FWrite(fh, &fs->fileDate, sizeof(fs->fileDate), 1);
	FWrite(fh, &fs->staticContents, sizeof(fs->staticContents), 1);
	
	ReserveArray(fs->stringsArray, fs->numSymbols * 8);
	
	stringOffset = SizeOfArray(fs->stringsArray);
	
	FWrite(fh, &fs->numSymbols, sizeof(fs->numSymbols), 1);
	for (i = 0; i < fs->numSymbols; i++)
	{
		STRPTR name = symbol->name;
		
		nameLen = 0;
		if (name)
		{
			nameLen = strlen(name) + 1;
		
			AppendArrayElements(fs->stringsArray, nameLen);
			CopyMem(name, &ArrayValues(TEXT, fs->stringsArray)[stringOffset], nameLen);
		
			symbol->name = (STRPTR)stringOffset;
			stringOffset += nameLen;
		}
		
		FWrite(fh, (UBYTE *)symbol, sizeof(struct Symbol), 1);
		symbol->name = name;
		symbol++;
	}

	nameLen = strlen(fs->filePath) + 1;
	AppendArrayElements(fs->stringsArray, nameLen);
	CopyMem(fs->filePath, &ArrayValues(TEXT, fs->stringsArray)[stringOffset], nameLen);
	FWrite(fh, &stringOffset, sizeof(ULONG), 1);
	// NB increment stringOffset if we ever write more strings
		
	FWrite(fh, &SizeOfArray(fs->includesArray), sizeof(ULONG), 1);
	FWrite(fh, ArrayValues(UBYTE, fs->includesArray),
		sizeof(UWORD), SizeOfArray(fs->includesArray));
		
	FWrite(fh, &SizeOfArray(fs->scopeStartsArray), sizeof(ULONG), 1);
	FWrite(fh, ArrayValues(UBYTE, fs->scopeStartsArray),
		sizeof(SymbolRef), SizeOfArray(fs->scopeStartsArray));
		
	FWrite(fh, &SizeOfArray(fs->sortedScopeEndsArray), sizeof(ULONG), 1);
	FWrite(fh, ArrayValues(UBYTE, fs->sortedScopeEndsArray),
		sizeof(SymbolRef), SizeOfArray(fs->sortedScopeEndsArray));
	
	WriteMap(fs->definitionValues, fh);
	WriteMap(fs->definitionArgs, fh);

	WriteMap(fs->nameMap, fh);

	FWrite(fh, &SizeOfArray(fs->stringsArray), sizeof(ULONG), 1);
	FWrite(fh, ArrayValues(UBYTE, fs->stringsArray),
		SizeOfArray(fs->stringsArray), 1);
		
	fs->strings = ArrayValues(TEXT, fs->stringsArray);
	
	Close(fh);
}

static struct FileSymbols *loadFileSymbols(struct SymDb *sdb, ULONG fileID)
{
	struct FileSymbols *fs;
	BPTR fh;
	BPTR oldDir;
	TEXT dbFileName[8];
	BPTR codeIndexDirLock;
	STRPTR pathLocation;
	ULONG size;
	ULONG i;
	struct Symbol *symbol;

	codeIndexDirLock = LockCodeIndexDirOfFileID(fileID);
	
	oldDir = CurrentDir(codeIndexDirLock);
	
	SNPrintf(dbFileName, 8, "%04lx.db", fileID);
	fh = Open(dbFileName, MODE_OLDFILE);
	CurrentDir(oldDir);
	UnLock(codeIndexDirLock);

	if (! fh)
		return NULL;
		
	fs = AllocMem(sizeof(struct FileSymbols), MEMF_CLEAR);
	
	if (! fs)
		goto fail_loadFileSymbols;
		 
	FRead(fh, &fs->fileSize, sizeof(fs->fileSize), 1);
	FRead(fh, &fs->fileDate, sizeof(fs->fileDate), 1);
	FRead(fh, &fs->staticContents, sizeof(fs->staticContents), 1);
	FRead(fh, &fs->numSymbols, sizeof(fs->numSymbols), 1);

	fs->symbols = AllocVec(fs->numSymbols * sizeof(struct Symbol), MEMF_ANY);
	if (! fs->symbols)
		goto fail_loadFileSymbols;
	FRead(fh, (UBYTE *)fs->symbols, fs->numSymbols * sizeof(struct Symbol), 1);
	
	fs->fileID = fileID;
	
	FRead(fh, &pathLocation, sizeof(ULONG), 1);

	FRead(fh, &size, sizeof(ULONG), 1);
	fs->includes = AllocVec(size * sizeof(UWORD), MEMF_ANY);
	if (! fs->includes)
		goto fail_loadFileSymbols;
	FRead(fh, (UBYTE *)fs->includes, size * sizeof(UWORD), 1);
		
	FRead(fh, &size, sizeof(ULONG), 1);
	fs->scopeStarts = AllocVec(size * sizeof(SymbolRef), MEMF_ANY);
	if (! fs->scopeStarts)
		goto fail_loadFileSymbols;
	FRead(fh, fs->scopeStarts,	size * sizeof(SymbolRef), 1);
		
	FRead(fh, &size, sizeof(ULONG), 1);
	fs->sortedScopeEnds = AllocVec(size * sizeof(SymbolRef), MEMF_ANY);
	if (! fs->sortedScopeEnds)
		goto fail_loadFileSymbols;
	FRead(fh, fs->sortedScopeEnds, size * sizeof(SymbolRef), 1);

	fs->definitionValues = ReadMap(fh);
	fs->definitionArgs = ReadMap(fh);
	
	fs->nameMap = ReadMap(fh);

	FRead(fh, &size, sizeof(ULONG), 1);
	fs->strings = AllocVec(size, MEMF_ANY);
	if (! fs->strings)
		goto fail_loadFileSymbols;
	FRead(fh, fs->strings, size, 1);

	Close(fh);
	fh = NULL;
		
	symbol = fs->symbols;
	for (i = 0; i < fs->numSymbols; i++)
	{
		if (symbol->name)
			symbol->name += (ULONG)fs->strings;
		
		symbol++;
	}
	pathLocation += (ULONG)fs->strings;
	fs->filePath = AllocVec(strlen(pathLocation) + 1, MEMF_ANY);
	if (! fs->filePath)
		goto fail_loadFileSymbols;

	strcpy(fs->filePath, pathLocation);

	MapIntToValue(sdb->fileSymsByID, fileID, (ULONG)fs);
	MapStrToValue(sdb->fileSymsByFilename, fs->filePath, (ULONG)fs);

	return fs;
	
fail_loadFileSymbols:
	if (fh)
		Close(fh);
	destroyFileSymbols(fs);
	return NULL;
}

struct FileSymbols *GetFileSymbols(struct SymDb *sdb, ULONG fileID)
{
	struct FileSymbols *fs;
	
	fs = (struct FileSymbols *)ValueOfIntKey(sdb->fileSymsByID, fileID);

	if (! fs)
		fs = loadFileSymbols(sdb, fileID);
			
	return fs;
}

struct Symbol *GetSymbol(struct SymDb *sdb, SymbolRef fetchThis)
{
	struct FileSymbols *fs;
	
	fs = GetFileSymbols(sdb, SRFile(fetchThis));
	
	if (!fs)
		return NULL;

	if (fs->numSymbols <= SRSym(fetchThis))
		return NULL;
		
	return fs->symbols + SRSym(fetchThis);
}

ULONG GetIncludeFile(struct SymDb *sdb, ULONG fileID, ULONG includeIndex)
{
	struct FileSymbols *fs;

	fs = GetFileSymbols(sdb, fileID);
	
	if (!fs)
		return NoFileRef;
	
	return fs->includes[includeIndex];
}

int isIdInArray(SymbolRef *partOfs, SymbolRef id)
{
	while (*(partOfs) != EndSymbolRef)
	{
		if (*(partOfs++) == id)
			return TRUE;
	}
	return FALSE;
}

SymbolRef NamedCompound(struct SymDb *sdb, STRPTR name, SymbolRef *scopeRefs)
{
	struct Symbol *sym;
	ULONG includedScopeRefs[2] = {0, EndSymbolRef};
	ULONG includedFile;
	ULONG includeIndex = 0;
	SymbolRef fetchThis = scopeRefs[0];
	UWORD currentFile = SRFile(fetchThis);

	if (sdb->lookupRunDepth == 0)
		ClearMap(sdb->lookupRunMap);
	AddIntKey(sdb->lookupRunMap, currentFile);
		
	while (sym = GetSymbol(sdb, fetchThis))
	{
		if (sym->name)
		{
			if (sym->typeId) // also works if compound type as it selfrefers
				if(strcmp(sym->name, name) == 0)
				{
					if (isIdInArray(scopeRefs, sym->partOf))
						return sym->typeId;			
				}
		}
		SRSym(fetchThis)++;
	}

	while ((includedFile = GetIncludeFile(sdb, currentFile, includeIndex)) != NoFileRef)
	{
		SymbolRef retval;
		if (! ContainsIntKey(sdb->lookupRunMap, includedFile))
		{
			includedScopeRefs[0] = (includedFile<<16) | 0;
			sdb->lookupRunDepth++;
			retval = NamedCompound(sdb, name, includedScopeRefs);
			sdb->lookupRunDepth--;
			if (retval != EndSymbolRef)
				return retval;
		}	
		includeIndex++;
	}
	
	return EndSymbolRef;
}

SymbolRef NamedKind(struct SymDb *sdb, STRPTR name, UBYTE kind, SymbolRef *scopeRefs)
{
	struct Symbol *sym;
	ULONG includedScopeRefs[2] = {0, EndSymbolRef};
	ULONG includedFile;
	ULONG includeIndex = 0;
	SymbolRef fetchThis = scopeRefs[0];
	UWORD currentFile = SRFile(fetchThis);
	struct FileSymbols *fs;
	ULONG symOffset;
	
	fs = GetFileSymbols(sdb, currentFile);
	
	if (! fs)
		return EndSymbolRef;

	if (sdb->lookupRunDepth == 0)
		ClearMap(sdb->lookupRunMap);
	AddIntKey(sdb->lookupRunMap, currentFile);
	
	symOffset = ValueOfStrKey(fs->nameMap, name);

	if (symOffset)
	{
		sym = fs->symbols + symOffset;
		if (sym->kind == kind) // also works if compound type as it selfrefers
			if (isIdInArray(scopeRefs, sym->partOf))
				return (((ULONG)currentFile)<<16) | symOffset;

		while (sym = GetSymbol(sdb, fetchThis))
		{
			if (sym->name)
			{
				if (sym->kind == kind) // also works if compound type as it selfrefers
					if(strcmp(sym->name, name) == 0)
					{
						if (isIdInArray(scopeRefs, sym->partOf))
							return fetchThis;			
					}
			}
			SRSym(fetchThis)++;
		}
	}

	while ((includedFile = GetIncludeFile(sdb, currentFile, includeIndex)) != NoFileRef)
	{
		SymbolRef retval;
		
		if (! ContainsIntKey(sdb->lookupRunMap, includedFile))
		{
			includedScopeRefs[0] = (includedFile<<16) | 0;
			sdb->lookupRunDepth++;
			retval = NamedKind(sdb, name, kind, includedScopeRefs);
			sdb->lookupRunDepth--;
			if (retval != EndSymbolRef)
				return retval;
		}	
		includeIndex++;
	}
	
	return EndSymbolRef;
}

void ListMembers(struct SymDb *sdb, Array retvals, SymbolRef compoundRef)
{
	struct Symbol *sym;
	SymbolRef fetchThis = compoundRef;
		
	SRSym(fetchThis) = 0;
	
	while (sym = GetSymbol(sdb, fetchThis))
	{		
		if (sym->partOf == compoundRef)
			AppendToArray(SymbolRef, retvals, fetchThis);

		SRSym(fetchThis)++;
	}
}

SymbolRef *ListNonMembers(struct SymDb *sdb, Array retvals, SymbolRef *scopeRefs, UWORD currentFile)
{
	struct Symbol *sym;
	ULONG includedScopeRefs[2] = {0, EndSymbolRef};
	ULONG includedFile;
	ULONG includeIndex = 0;
	SymbolRef fetchThis;
	
	SRFile(fetchThis) = currentFile;
	SRSym(fetchThis) = 0;
	
	while (sym = GetSymbol(sdb, fetchThis))
	{
		if (sym->name && sym->kind != SDB_MEMBER)
		{
			if (isIdInArray(scopeRefs, sym->partOf))
				AppendToArray(SymbolRef, retvals, fetchThis);
		}
		SRSym(fetchThis)++;
	}

	while ((includedFile = GetIncludeFile(sdb, currentFile, includeIndex)) != NoFileRef)
	{
		includedScopeRefs[0] = (includedFile<<16) | 0;
		ListNonMembers(sdb, retvals, includedScopeRefs, includedFile);
		includeIndex++;
	}
	
	return retvals;
}

void FilterByName(struct SymDb *sdb, Array candidateIds, STRPTR leadStr)
{
	SymbolRef *outputId = ArrayValues(SymbolRef, candidateIds);
	size_t leadLen = strlen(leadStr);
	LONG newSize = 0;
	
	ArrayForEach(SymbolRef, inputId, candidateIds,
	{
		struct Symbol *sym = GetSymbol(sdb, inputId);
		
		if (sym->name && strncmp(sym->name, leadStr, leadLen) == 0)
		{
			*(outputId++) = inputId;
			newSize++;
		}
	});
	
	ResizeArray(candidateIds, newSize);
}


STRPTR NameOfId(struct SymDb *sdb, SymbolRef id)
{
	struct Symbol *sym = GetSymbol(sdb, id);
	
	if (!sym)
		return NULL;
		
	return sym->name;
}

SymbolRef *GetScopeStarts(struct SymDb *sdb, UWORD fileID)
{
	struct FileSymbols *fs;

	fs = GetFileSymbols(sdb, fileID);
	
	if (!fs)
		return NULL;
	
	return fs->scopeStarts;
}

SymbolRef *GetScopeEnds(struct SymDb *sdb, UWORD fileID)
{
	struct FileSymbols *fs;

	fs = GetFileSymbols(sdb, fileID);
	
	if (!fs)
		return NULL;
	
	return fs->sortedScopeEnds;
}


void DetermineScope(struct SymDb *sdb, Array resultScopeRefs, UWORD fileID, ULONG blockNum)
{
	SymbolRef *beginRefs;
	SymbolRef *endRefs;

	beginRefs = GetScopeStarts(sdb, fileID);
	endRefs = GetScopeEnds(sdb, fileID);

	// Now build the answer by taking all the starts before
	// or at our block number, but not those that are also
	// already stopped again
		
	while (*beginRefs != EndSymbolRef)
	{
		struct Symbol *beginSym = GetSymbol(sdb, *beginRefs);
		struct Symbol *endSym = GetSymbol(sdb, *endRefs);

		if (beginSym->blockNum > blockNum)
			break;
		
		if (endSym->blockNum >= blockNum)
			AppendToArray(SymbolRef, resultScopeRefs, *beginRefs);

		endRefs++;		
		beginRefs++;
	}
	
	AppendToArray(SymbolRef, resultScopeRefs, EndSymbolRef);
}

BPTR LockCodeIndex(struct SymDb *sdb, STRPTR filePath)
{
	BPTR codeIndexDir = NULL;
	BPTR lock;
	BPTR oldDir;

	lock = Lock(filePath, SHARED_LOCK);
	if (lock)
	{
		BPTR pLock = ParentDir(lock);
		UnLock(lock);
		
		if (pLock)
		{
			oldDir = CurrentDir(pLock);
			
			while (pLock && !codeIndexDir)
			{
				codeIndexDir = Lock(".codeidx", SHARED_LOCK);

				pLock = ParentDir(pLock);
				if (pLock)
					UnLock(CurrentDir(pLock));
			}
			UnLock(CurrentDir(oldDir));
		}
	}
	return codeIndexDir;
}

BPTR LockCodeIndexDirOfFileID(UWORD fileID)
{
	BPTR fh;
	ULONG success;
	ULONG nameOffset;
	TEXT areaName[256];
	ULONG offset;
	
	// we need to find the area in the global arealist
	fh = Open("Codeindex:fileAreas", MODE_OLDFILE);
	if (!fh)
		return NULL;

	// Quickly calculate offset ((fileID>>4)-1) * 4
	offset = 0xFFF0 & fileID;
	offset = (offset>>2) - 4;
	Seek(fh, offset, OFFSET_BEGINNING);
	success = Read(fh, &nameOffset, 4);
	Close(fh);
	if (! success)
		return NULL;


	// we need to lookup the areaname
	fh = Open("Codeindex:fileAreaNames", MODE_OLDFILE);
	if (!fh)
		return NULL;

	Seek(fh, nameOffset, OFFSET_BEGINNING);
	success = (ULONG)FGets(fh, areaName, sizeof(areaName));
	Close(fh);
	if (! success)
		return NULL;

	areaName[strlen(areaName) - 1] = '\0';
	
	if (AddPart(areaName, ".codeidx", sizeof(areaName)))
		return Lock(areaName, SHARED_LOCK);
	return NULL;
}

// Also takes care of a new file area if needed
UWORD GenerateFileID(struct SymDb *sdb, BPTR codeIndexDir, STRPTR filePath)
{
	BPTR oldDir;
	BPTR fh;
	UWORD fileID = NoFileRef;
	int newArea = FALSE;
	Map nameOfIDMap;

	if (!codeIndexDir)
		return NoFileRef;
		
	oldDir = CurrentDir(codeIndexDir);
	fh = Open("highestfileid", MODE_OLDFILE);
	
	if (fh)
	{
		Read(fh, &fileID, 2);
		Close(fh);
		if ((fileID & 0xF) == 0xF)
			newArea = TRUE;
	}
	else
		newArea = TRUE;
		
	if (newArea)
	{
		TEXT areaName[256];
		ULONG nameOffset;
		
		// we need the areaname
		fh = Open("areaname", MODE_OLDFILE);
		*areaName = 0;
		if (!fh)
			goto GenerateFileID_cleanup;

		FGets(fh, areaName, 256);
		Close(fh);

		// we need to write the areaname to the global list
		fh = Open("Codeindex:fileAreaNames", MODE_READWRITE);
		if (!fh)
			goto GenerateFileID_cleanup;

		Seek(fh, 0, OFFSET_END);
		nameOffset = Seek(fh, 0, OFFSET_END);
		FPuts(fh, areaName);
		FPutC(fh, '\n');
		Close(fh);

		// we need to add a new area and link to the name in the global arealist
		fh = Open("Codeindex:fileAreas", MODE_READWRITE);
		if (!fh)
			goto GenerateFileID_cleanup;

		Seek(fh, 0, OFFSET_END);
		fileID = Seek(fh, 0, OFFSET_END);
		Write(fh, &nameOffset, 4);
		Close(fh);
		
		 // Fast way of doing: fileID = ((offset / 4) + 1) * 16
		fileID *= 4;
		fileID += 16;
	}
	else
		fileID++;

	// update our highestfileid
	fh = Open("highestfileid", MODE_NEWFILE);
	if (!fh)
		goto GenerateFileID_cleanup;

	Write(fh, &fileID, 2);
	Close(fh);
		
	// Finally write filepath->fileid to "nameofid.map"
	fh = Open("nameofid.map", MODE_OLDFILE);
	if (!fh)
		goto GenerateFileID_cleanup;

	nameOfIDMap = ReadMap(fh);
	Close(fh);

	MapStrToValue(nameOfIDMap, filePath, fileID);
	
	fh = Open("nameofid.map", MODE_NEWFILE);
	if (!fh)
		goto GenerateFileID_cleanup;

	WriteMap(nameOfIDMap, fh);
	Close(fh);
	
GenerateFileID_cleanup:
	CurrentDir(oldDir);
	return fileID;
}

UWORD FindFileID(struct SymDb *sdb, STRPTR filePath)
{
	struct FileSymbols *fs;
	UWORD fileID = NoFileRef;
	BPTR codeIndexDir;
	TEXT codeIndexDirPath[512];
	
	fs = (struct FileSymbols *)ValueOfStrKey(sdb->fileSymsByFilename, filePath);

	if (fs)
		return fs->fileID;

	// Find the codeindex dir in one of the ancestor dirs
	codeIndexDir = LockCodeIndex(sdb, filePath);

	if (codeIndexDir)
	{
		BPTR oldDir;
		BPTR fh;

		NameFromLock(codeIndexDir, codeIndexDirPath, sizeof(codeIndexDirPath));
		
		*FilePart(codeIndexDirPath) = 0;
		
		if (strncmp(filePath, codeIndexDirPath, strlen(codeIndexDirPath)) == 0)
			filePath += strlen(codeIndexDirPath);
			
		// Try and load "nameofid.map"
		oldDir = CurrentDir(codeIndexDir);
		fh = Open("nameofid.map", MODE_OLDFILE);
		CurrentDir(oldDir);
		
		if (fh)
		{
			Map nameOfIDMap = ReadMap(fh);
			if (nameOfIDMap)
			{
				fileID = ValueOfStrKey(nameOfIDMap, filePath);
	
				DeleteMap(nameOfIDMap);
			
				if (!fileID)
					fileID = NoFileRef;
			}
			Close(fh);
		}
	}
				
	return fileID;
}



STRPTR kindName[] = {"INSTANCE", "MEMBER", "SCOPE", "COMPOUND", "SCOPEEND", "TYPE", "DEFINTION"};

SymbolRef AddSymbol(struct SymDb *sdb, STRPTR name, SymbolRef partOf, SymbolRef typeId, ULONG kind, ULONG blockNum)
{
	struct FileSymbols *fs;
	struct Symbol *s;
	ULONG fileID = SRFile(partOf);
	SymbolRef thisId;

	fs = GetFileSymbols(sdb, fileID);
	
	if (!fs)
		return NULL;
		
	if (fs->numSymbols == 1999)
		return NULL;
		
	s = fs->symbols + fs->numSymbols;
	
	thisId = (fileID << 16) | fs->numSymbols;
	
	if (kind == SDB_COMPOUND)
		typeId = thisId;
	s->name = name;
	s->partOf = partOf;
	s->typeId = typeId;
	s->kind = kind;
	s->blockNum = blockNum;

	if (sdb->verbosity > 1)
		printf("%08lx Line %3ld: %s: %s part of %08lx and of type %08lx\n", thisId ,blockNum, kindName[kind],
			 name ? name : (STRPTR)"", partOf, typeId);

	if (name)
		MapStrToValue(fs->nameMap, name, fs->numSymbols);
	fs->numSymbols++;
	
	if (kind == SDB_SCOPE)
	{
		ArrayBackValue(SymbolRef, fs->scopeStartsArray) = thisId;
		// then terminate and refresh plain data pointer
		AppendToArray(SymbolRef, fs->scopeStartsArray, EndSymbolRef);
		fs->scopeStarts = ArrayValues(SymbolRef, fs->scopeStartsArray);

		// Then ends array should grow syncroniously
		// Also remember to refresh plain data pointer
		AppendToArray(SymbolRef, fs->sortedScopeEndsArray, EndSymbolRef);
		fs->sortedScopeEnds = ArrayValues(SymbolRef, fs->sortedScopeEndsArray);
	}
	if (kind == SDB_SCOPEEND)
	{
		ULONG i = 0;
		ULONG size = SizeOfArray(fs->scopeStartsArray);
		
		// find index of partOf in scopeStartsArray
		while (i < size)
		{
			if (ArrayValues(SymbolRef, fs->scopeStartsArray)[i] == partOf)
				break;
			i++;
		}
		// change sortedScopeEndsArray at index
		if (i < size)
			ArrayValues(SymbolRef, fs->sortedScopeEndsArray)[i] = thisId;
	}
	
	return thisId;
}

void AddInclude(struct SymDb *sdb, SymbolRef currentFileScope, UWORD includeFileRef)
{
	struct FileSymbols *fs;

	fs = GetFileSymbols(sdb, SRFile(currentFileScope));
	
	if (!fs)
		return;

	ArrayBackValue(UWORD, fs->includesArray) = includeFileRef;

	// then terminate and refresh plain data pointer
	AppendToArray(UWORD, fs->includesArray, NoFileRef);	
	fs->includes = ArrayValues(UWORD, fs->includesArray);
}

void StoreDefinitionValue(APTR sdb, SymbolRef ref, STRPTR value)
{
	struct FileSymbols *fs;
	ULONG stringOffset;
	int nameLen = strlen(value) + 1;

	fs = GetFileSymbols(sdb, SRFile(ref));
	
	if (!fs)
		return;
		
	stringOffset = SizeOfArray(fs->stringsArray);
	AppendArrayElements(fs->stringsArray, nameLen);
	fs->strings = ArrayValues(TEXT, fs->stringsArray);

	CopyMem(value, &fs->strings[stringOffset], nameLen);
	
	MapIntToValue(fs->definitionValues, ref, stringOffset);
}

void StoreDefinitionArgs(APTR sdb, SymbolRef ref, STRPTR args)
{
	struct FileSymbols *fs;
	ULONG stringOffset;
	int nameLen = strlen(args) + 1;

	fs = GetFileSymbols(sdb, SRFile(ref));
	
	if (!fs)
		return;
		
	stringOffset = SizeOfArray(fs->stringsArray);
	AppendArrayElements(fs->stringsArray, nameLen);
	fs->strings = ArrayValues(TEXT, fs->stringsArray);

	CopyMem(args, &fs->strings[stringOffset], nameLen);
	
	MapIntToValue(fs->definitionArgs, ref, stringOffset);
}

STRPTR GetDefinitionValue(APTR sdb, SymbolRef ref)
{
	struct FileSymbols *fs;

	fs = GetFileSymbols(sdb, SRFile(ref));
	
	if (!fs)
		return NULL;
		
	return fs->strings + ValueOfIntKey(fs->definitionValues, ref);
}

STRPTR GetDefinitionArgs(APTR sdb, SymbolRef ref)
{
	struct FileSymbols *fs;
	ULONG index;

	fs = GetFileSymbols(sdb, SRFile(ref));
	
	if (!fs)
		return NULL;
		
	index = ValueOfIntKey(fs->definitionArgs, ref);
	
	if (index)
		return fs->strings + index;
		
	return NULL;
}

void ChangeSymbolKind(struct SymDb *sdb, SymbolRef symRef, ULONG newKind)
{
	struct Symbol *sym = GetSymbol(sdb, symRef);
	struct FileSymbols *fs;
	
	if (sym)
		sym->kind = newKind;
		
	fs = GetFileSymbols(sdb, SRFile(symRef));

	if (sdb->verbosity > 1)
		printf("Change %08lx to %s\n", symRef, kindName[newKind]);

	if (newKind == SDB_SCOPE)
	{
		ArrayBackValue(SymbolRef, fs->scopeStartsArray) = symRef;
		// then terminate and refresh plain data pointer
		AppendToArray(SymbolRef, fs->scopeStartsArray, EndSymbolRef);
		fs->scopeStarts = ArrayValues(SymbolRef, fs->scopeStartsArray);
	
		// Then ends array should grow syncroniously
		// Also remember to refresh plain data pointer
		AppendToArray(SymbolRef, fs->sortedScopeEndsArray, EndSymbolRef);
		fs->sortedScopeEnds = ArrayValues(SymbolRef, fs->sortedScopeEndsArray);
	}
}

void DeleteTrailingSymbols(struct SymDb *sdb, SymbolRef lastKeptSymRef)
{
	struct FileSymbols *fs;
	ULONG i;
	ULONG lastKeptSymIndex = SRSym(lastKeptSymRef);
	
	fs = GetFileSymbols(sdb, SRFile(lastKeptSymRef));

	i = fs->numSymbols - 1;
	while (i > lastKeptSymIndex)
	{
		if (fs->symbols[i].name)
			RemoveStrKey(fs->nameMap, fs->symbols[i].name);
		i--;
	}
	
	if (sdb->verbosity > 1)
		printf("Last Symbol is now %08lx\n", lastKeptSymRef);

	fs->numSymbols = lastKeptSymIndex + 1;
}

struct FileSymbols *createFileSymbolsForAdding(struct SymDb *sdb, 
 SymbolRef startScope, ULONG fileID, ULONG numSymbolsReserved)
{
	struct FileSymbols *fs = AllocMem(sizeof(struct FileSymbols), MEMF_CLEAR);
	
	if (! fs)
		return NULL;
		 
	fs->symbols = AllocVec(numSymbolsReserved * sizeof(struct Symbol), MEMF_CLEAR);
	fs->fileID = fileID;
	MapIntToValue(sdb->fileSymsByID, fileID, (ULONG)fs);
	
	fs->stringsArray = NewArray(0);
	AppendToArray(TEXT, fs->stringsArray, 0);
	fs->strings = ArrayValues(TEXT, fs->stringsArray);
	
	// init and terminate
	fs->sortedScopeEndsArray = NewArray(2);
	AppendToArray(SymbolRef, fs->sortedScopeEndsArray, EndSymbolRef);

	// init and terminate
	fs->scopeStartsArray = NewArray(2);
	AppendToArray(SymbolRef, fs->scopeStartsArray, EndSymbolRef);

	// add scope (refreshes plain  pointers)
	AddSymbol(sdb, NULL, startScope, 0, SDB_SCOPE, 0);

	// init, terminate and refresh plain pointer
	fs->includesArray = NewArray(1);
	AppendToArray(UWORD, fs->includesArray, NoFileRef);	
	fs->includes = ArrayValues(UWORD, fs->includesArray);
	
	fs->definitionValues = NewMap(CNTKIT_KEY_ORDINAL, CNTKIT_VALUESIZE, 4, TAG_DONE);
	
	fs->definitionArgs = NewMap(CNTKIT_KEY_ORDINAL, CNTKIT_VALUESIZE, 4, TAG_DONE);
	
	fs->nameMap = NewMap(CNTKIT_KEY_STRING, CNTKIT_CAPACITY, 200, CNTKIT_VALUESIZE, 4, TAG_DONE);

	return fs;
}


SymbolRef StartCodeFile(struct SymDb *sdb, STRPTR filePath, ULONG fileSize, struct DateStamp *fileTimeStamp, ULONG staticContents, ULONG fileID)
{
	SymbolRef startScope;
	struct FileSymbols *fs;
	BPTR codeIndexDirLock;
		
	codeIndexDirLock = LockCodeIndex(sdb, filePath);
	
	if (fileID == NoFileRef)
		fileID = GenerateFileID(sdb, codeIndexDirLock, filePath);
	
	if (fileID == NoFileRef)
	{
		UnLock(codeIndexDirLock);
		return EndSymbolRef;
	}
		
	startScope = fileID << 16;

	fs = createFileSymbolsForAdding(sdb, startScope, fileID, 2000);

	if (! fs)
	{
		UnLock(codeIndexDirLock);
		return EndSymbolRef;
	}

	fs->fileSize = fileSize;
	fs->fileDate = *fileTimeStamp;
	fs->staticContents = staticContents;
		
	//map it now and not in finalize - prevents accidental recursive parsing
	MapStrToValue(sdb->fileSymsByFilename, filePath, (ULONG)fs);
	
	fs->filePath = AllocVec(strlen(filePath)+1, MEMF_ANY);
	if (fs->filePath)
		strcpy(fs->filePath, filePath);
	
	fs->codeIndexDirLock = codeIndexDirLock;

	return startScope;
}

UWORD FinalizeCodeFile(struct SymDb *sdb, SymbolRef startScope, ULONG blockNum)
{
	struct FileSymbols *fs;
	ULONG fileID = SRFile(startScope);

	AddSymbol(sdb, NULL, startScope, 0, SDB_SCOPEEND, blockNum);

	fs = GetFileSymbols(sdb, fileID);
	
	if (!fs)
		return NoFileRef;

	saveFileSymbols(fs);
	
	UnLock(fs->codeIndexDirLock);
	fs->codeIndexDirLock = NULL;

	destroyFileSymbols(fs);
	fs = loadFileSymbols(sdb, fileID);

	return (UWORD)fileID;
}

// Recursively checks if things are obsolete
BOOL IsObsolete(struct SymDb *sdb, ULONG fileID, struct FileInfoBlock *fib, ULONG skipStatic)
{
	struct FileSymbols *fs;
	ULONG includedScopeRefs[2] = {0, EndSymbolRef};
	ULONG includedFile;
	ULONG includeIndex = 0;
	BOOL ownFib = FALSE;
	BOOL obsolete;			

	fs = GetFileSymbols(sdb, fileID);
	
	if (! fs)
		return TRUE;
		
	if (skipStatic && fs->staticContents)
		return FALSE;
	
	if (!fib)
	{
		BPTR codeIndexDirLock, baseDir, oldDir, file;
		
		fib = AllocDosObject(DOS_FIB, NULL);

		codeIndexDirLock = LockCodeIndexDirOfFileID(fileID);
		baseDir = ParentDir(codeIndexDirLock);
		UnLock(codeIndexDirLock);
	
		oldDir = CurrentDir(baseDir);
		
		file = Lock(fs->filePath, SHARED_LOCK);
		Examine(file, fib);
		UnLock(file);
		
		CurrentDir(oldDir);
		UnLock(baseDir);

		ownFib = TRUE;
	}
	
	obsolete = ((fs->fileSize != fib->fib_Size) || (fs->fileDate.ds_Days != fib->fib_Date.ds_Days)
	 || (fs->fileDate.ds_Minute != fib->fib_Date.ds_Minute)  || (fs->fileDate.ds_Tick != fib->fib_Date.ds_Tick));

	if (ownFib)	
		FreeDosObject(DOS_FIB, fib);

	if (obsolete)
		return TRUE;

	if (sdb->lookupRunDepth == 0)
		ClearMap(sdb->lookupRunMap);
	AddIntKey(sdb->lookupRunMap, fileID);

	while ((includedFile = GetIncludeFile(sdb, fileID, includeIndex)) != NoFileRef)
	{
		if (! ContainsIntKey(sdb->lookupRunMap, includedFile))
		{
			includedScopeRefs[0] = (includedFile<<16) | 0;
			sdb->lookupRunDepth++;
			obsolete = IsObsolete(sdb, includedFile, NULL, skipStatic);
			sdb->lookupRunDepth--;
			if (obsolete)
				return TRUE;
		}	
		includeIndex++;
	}

	return FALSE;
}

struct CompilerDefinition
{
	STRPTR name;
	STRPTR value;
} compilerDefs[15] =
{
	"_AMIGA", "1",
	"_M68000", "1",
	"__SASC", "1",
	"__SASC_60", "1",
	"__SASC_650", "1",
	"__VERSION__", "6",
	"__REVISION__", "50",
	"__STDC__", "1",
	"__FILE__", "0",
	"__LINE__", "0",
	"__FUNC__", "0",
	"__DATE__", "202009",
	"__TIME__", "1200",
	0,
};

/* THE following are produced depending on compiler options
AMIGA noansi
LPTR noansi and noshortint
SPTR noansi and shortint
LATTICE noansi
LATTICE 50 noansi
_M68010 cpu=68010 | 68020 | 68030 | 68040
_M68020 cpu =68020|68030 | 68040
_M68030 cpu=68030 | 68040
_M68040 cpu=68040
_M68881 math=68881
_FFP math=FFP
_IEEE math=IEEE
_DEBUG debug=any-value
_SHORTINT shortint
_UNSCHAR unschar
_OPT optimize
_OPTINLINE optinline
_PPONLY pponly
_PROFILE profile
_GENPROTO genproto
_MGST makegst
_GST gst
*/

void addCompilerDef(struct SymDb *sdb, struct CompilerDefinition *cDef, SymbolRef scope)
{
	SymbolRef ref;
	
	ref = AddSymbol(sdb, cDef->name, scope, 0, SDB_DEFINITION, 1);
	
	StoreDefinitionValue(sdb, ref, cDef->value);
}

void CreateCompilerDefinitions(struct SymDb *sdb)
{
	SymbolRef startScope;
	ULONG fileID;
	struct FileSymbols *fs;
	struct CompilerDefinition *cDef = compilerDefs;
		
	fileID = 0x0000;
			
	startScope = 0x00000000;
	
	fs = createFileSymbolsForAdding(sdb, startScope, fileID, 30);
	
	if (! fs)
		return;
		
	while (cDef->name)
		addCompilerDef(sdb, cDef++, startScope);

	AddSymbol(sdb, NULL, startScope, 0, SDB_SCOPEEND, 2);
}

APTR __ASM__ CreateSymDb(__REG__(d0, LONG verbosity))
{
	struct SymDb *sdb = AllocMem(sizeof(struct SymDb), MEMF_CLEAR);

	ContainerkitBase = OpenLibrary("containerkit.library", 1);

	sdb->fileSymsByFilename = NewMap(CNTKIT_KEY_STRING, CNTKIT_VALUESIZE, 4, TAG_DONE);
	sdb->fileSymsByID = NewMap(CNTKIT_KEY_ORDINAL, CNTKIT_VALUESIZE, 4, TAG_DONE);
	sdb->verbosity = verbosity;

	sdb->lookupRunMap = NewMap(CNTKIT_KEY_ORDINAL, TAG_DONE);
	CreateCompilerDefinitions(sdb);

	return sdb;
}

void __ASM__ DestroySymDb(__REG__(a0, struct SymDb *sdb))
{
	MapIter iter;
	struct FileSymbols *fs;
	
	if (! sdb)
		return;
		
	InitMapIter(iter, sdb->fileSymsByID);
	
	while(! AtEndMapIter(iter))
	{
		fs = *PtrToValueOfMapIter(struct FileSymbols *, iter);
		destroyFileSymbols(fs);
		AdvanceMapIter(iter);
	}

	DeleteMap(sdb->fileSymsByFilename);
	DeleteMap(sdb->fileSymsByID);

	DeleteMap(sdb->lookupRunMap);

	FreeMem(sdb, sizeof(struct SymDb));
	
	CloseLibrary(ContainerkitBase);
}

void __ASM__ CreateCodeIndexProject(__REG__(a0, STRPTR absolutePath))
{
	BPTR oldDir;
	BPTR projectDir;
	BPTR indexDir;
	
	projectDir = Lock(absolutePath, SHARED_LOCK);
	
	if (!projectDir)
		return;
		
	oldDir = CurrentDir(projectDir);
	
	indexDir = Lock(".codeidx", SHARED_LOCK);

	if (!indexDir)
	{
		indexDir = CreateDir(".codeidx");
		
		UnLock(indexDir);
		
		indexDir = Lock(".codeidx", SHARED_LOCK);
		
		if (indexDir)
		{
			BPTR fh;
			Map map;
			
			CurrentDir(indexDir);
			
			DeleteFile("highestfileid");
			
			fh = Open("areaname", MODE_NEWFILE);
			FPuts(fh, absolutePath);
			Close(fh);
			
			fh = Open("nameofid.map", MODE_NEWFILE);
			map = NewMap(CNTKIT_KEY_STRING,
				CNTKIT_CAPACITY, 16,
				CNTKIT_VALUESIZE, 4,
				TAG_DONE);
			WriteMap(map, fh);
			Close(fh); 
	
		}
	}	
	CurrentDir(oldDir);
	UnLock(projectDir);
	UnLock(indexDir);
}

STRPTR __ASM__ GetCompletionCommand(__REG__(a0, APTR abstractSdb), __REG__(a1, STRPTR contextStr), __REG__(a2, STRPTR leadStr), __REG__(a3, STRPTR filePath), __REG__(d0, ULONG blockNum))
{
	struct SymDb *sdb = (struct SymDb *)abstractSdb;
	STRPTR retval = NULL;
	Array candidateIds;
	Array scopeRefs;

	UWORD currentFile;
	
	if (!sdb)
		return NULL;
	
	currentFile = FindFileID(sdb, filePath);

	if (currentFile == 0xFFFF)
		return 0;

	candidateIds = NewArray(2);
	scopeRefs = NewArray(2);
	
	DetermineScope(sdb, scopeRefs, currentFile, blockNum);
	
	if (contextStr)
	{
		SymbolRef compoundType;
		
		compoundType = NamedCompound(sdb, contextStr, ArrayValues(SymbolRef, scopeRefs));

		ListMembers(sdb, candidateIds, compoundType);
	}
	else
	{
		ListNonMembers(sdb, candidateIds, ArrayValues(SymbolRef, scopeRefs), currentFile);
	}

	// Filter candidate (slow scan, so do last)
	if (leadStr)
		FilterByName(sdb, candidateIds, leadStr);

	// Turn candidates into names
	if (SizeOfArray(candidateIds) > 0)
	{
		STRPTR end;
		ULONG size = 13 + strlen(leadStr) +200;
		
		ArrayForEach(SymbolRef, ref, candidateIds,
		{
			size += 1 + strlen(NameOfId(sdb, ref));
		});
		
		retval = AllocVec(size, MEMF_ANY);

		strcpy(retval, "COMPLETE \"");
		if (leadStr)
			strcat(retval, leadStr);
		strcat(retval, "\"");

		end = retval + strlen(retval);

		ArrayForEach(SymbolRef, ref, candidateIds,
		{
			STRPTR name;

			*(end++) = ' '; // will 0 term in strcpy below

			name = NameOfId(sdb, ref);
			strcpy(end, name);
			end += strlen(name);
		});
	}
	
	DeleteArray(candidateIds);
	DeleteArray(scopeRefs);
	return retval;
}
