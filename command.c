#define __CLIB_PRAGMA_LIBCALL
#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include <containerkit/map.h>
#include <clib/containerkit_protos.h>
#include <pragmas/containerkit_pragmas.h>

extern struct Library *ContainerkitBase;

#include <stdio.h>
#include <string.h>

#include "symdb_p.h"
#include "symdb.h"
#include "lvo_clib.h"
#define	__NOLIBBASE__

char *vers="\0$VER: CodeIndex "__AMIGADATE__""; // AMIGADATE is a SAS/C speciality
char *stacksize = "$STACK:1000000"; // only works when started from CLI

/* define your command template and options */
#define TEMPLATE       "VERBOSE/S,QUIET/S,PROJDIR/K/A,SUBPATH"
#define OPT_VERBOSE 0
#define OPT_QUIET 1
#define OPT_PROJECTBASE 2
#define OPT_PATH 3
#define OPT_COUNT 4


#define PATHBUFSIZE 2048

STRPTR searchPaths[] = 
{
	//"PROJECTDIR:projectdir",
	"NDK:include_h",
	NULL
};

STRPTR ExtOfName(STRPTR name)
{
	STRPTR dot = strrchr(name, '.');

	if (dot)
		return dot+1;
	
	return "";
}

void processPath(struct SymDb *sdb, BPTR lock, STRPTR path, struct ParseAuxInfo *pai)
{
	struct FileInfoBlock *fib;

	fib = AllocDosObject(DOS_FIB, TAG_DONE);
	
	if (! fib)
		return;

	Examine(lock, fib);
	
	if (fib->fib_DirEntryType > 0)
	{
		while (ExNext(lock, fib))
		{
			AddPart(path, fib->fib_FileName, PATHBUFSIZE);
			if (fib->fib_DirEntryType <= 0)
			{
				STRPTR ext = ExtOfName(fib->fib_FileName);
				if (Stricmp(ext, "c")== 0 || Stricmp(ext, "h")== 0)
				{
					pai->fileSize = fib->fib_Size;
					pai->dateStamp = &fib->fib_Date;
	
					ParseFile(sdb, path, NULL, pai);
				}
			}
			else
			{
				if (fib->fib_FileName[0] != '.')
				{
					BPTR oldDir;
					BPTR subLock;
					
					oldDir = CurrentDir(lock);
					subLock = Lock(fib->fib_FileName, SHARED_LOCK);
					CurrentDir(oldDir);
					if (subLock)
					{
						processPath(sdb, subLock, path, pai);
						UnLock(subLock);
					}
				}
			}
			*PathPart(path) = '\0';
		}
	}
	else
	{
		pai->fileSize = fib->fib_Size;
		pai->dateStamp = &fib->fib_Date;

		ParseFile(sdb, path, NULL, pai);
	}

	FreeDosObject(DOS_FIB, fib);
}


/*
  - MARKASSTATIC sets the StaticContents boolean to true
  - FORCE will not skip static contents includefiles
  - VERBOSE prints more info as it works through the files
  - PROJECTBASE creates a codeindex "project dir" at the path
  - path is a file or dir to be processed
  - if path is dir we recurse
*/
int main(int argc, char **argv)
{
	struct RDArgs *rdargs = NULL;
	LONG opts[OPT_COUNT+1] = { NULL };

	Printf("CodeIndex\n");
	if (argc == 2 && strcmp(argv[1], "clean")==0)
	{
		BPTR fh;
		
		fh = Open("Codeindex:fileAreaNames", MODE_OLDFILE);
		if (fh)
		{
			TEXT buffer[300];
			TEXT areaName[256];
			
			while(FGets(fh, areaName, sizeof(areaName)))
			{
				areaName[strlen(areaName) - 1] = '\0';
				sprintf(buffer, "DELETE %s/.codeidx ALL\n", areaName);
				SystemTags(buffer, TAG_DONE);
			}
			Close(fh);
			SystemTags("DELETE codeindex:#?", TAG_DONE);
		}
	}
	else if (rdargs = ReadArgs(TEMPLATE, opts, NULL))
	{
		struct SymDb *sdb;
		struct ParseAuxInfo pai;

		pai.NDKMode = TRUE;
		if (opts[OPT_VERBOSE])
			pai.verbosity = 2;
		else if (opts[OPT_QUIET])
			pai.verbosity = 0;
		else
			pai.verbosity = 1;
		
		pai.searchPaths = searchPaths;
		
		sdb = CreateSymDb(pai.verbosity);
		if (sdb)
		{
			BPTR projectDirLock;
			STRPTR projectBase = (STRPTR)opts[OPT_PROJECTBASE];
			TEXT fullPath[PATHBUFSIZE];
			
			projectDirLock = Lock(projectBase, SHARED_LOCK);
			
			if (projectDirLock)
			{
				BPTR oldDir;
				
				CreateCodeIndexProject(projectBase);
				
				fullPath[0] = '\0';

				if (opts[OPT_PATH])
					strcpy(fullPath, (STRPTR)opts[OPT_PATH]);
					
				oldDir = CurrentDir(projectDirLock);
				
				processPath(sdb, fullPath[0] ? Lock(fullPath, SHARED_LOCK) : projectDirLock, fullPath, &pai);
				
				CurrentDir(oldDir);
			}
			
			DestroySymDb(sdb);
		}
		else
			Printf("Error: Unable to start CodeIndex\n");
	}
	else
	{
		Printf("Error: Unable to parse arguments\n");
		return RETURN_ERROR;
	}
	return RETURN_OK;
}


