#ifndef CODEINDEX_CODEINDEX_H
#define CODEINDEX_CODEINDEX_H

#include <exec/types.h>
#include <dos/dos.h>

struct ParseAuxInfo
{
    STRPTR *searchPaths;
    LONG fileSize;
    struct DateStamp *dateStamp;
    BOOL NDKMode;
    BOOL verbosity;
};

#endif
