#include <clib/compiler-specific.h>
#include <proto/exec.h>

#include "codeindex/codeindex.h"

APTR __ASM__ CreateSymDb(__REG__(d0, LONG verbosity));
void __ASM__ DestroySymDb(__REG__(a0, APTR sdb));

void __ASM__ CreateCodeIndexProject(__REG__(a0, STRPTR absolutePath));
UWORD __ASM__ ParseFile(__REG__(a0, APTR sdb), __REG__(a1, STRPTR projRelFilePath), __REG__(a2, BPTR tmpDir), __REG__(a3, struct ParseAuxInfo *parseAuxInfo));

STRPTR __ASM__ GetCompletionCommand(__REG__(a0, APTR sdb), __REG__(a1, STRPTR contextStr), __REG__(a2, STRPTR leadStr), __REG__(a3, STRPTR filePath), __REG__(d0, ULONG blockNum));

