#include <exec/types.h>


#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "symdb.h"
#include "lvo_clib.h"

#include "lexer_C.h"

struct Parser
{
	struct Lexer *lexer;
	struct Token lookahead;
	SymbolRef currentType;
	SymbolRef declarationType;
	SymbolRef currentScope;
	SymbolRef fileScope;
	APTR sdb;
	STRPTR filePath;
	BPTR tmpDir;
	struct ParseAuxInfo *auxInfo;
};


void ParseCompound(struct Parser *parser, BOOL isEnum);
void ParseBlock(struct Parser *parser);
int ParseDecl(struct Parser *parser, BOOL allowCommas, ULONG sdbKind);
UWORD ParseFileImpl(APTR sdb, BPTR fileLock, STRPTR filePath, BPTR tmpDir, struct ParseAuxInfo *pai, ULONG fileID);
void ParseDefine(struct Parser *parser);
void ParseInclude(struct Parser *parser);
void ParseExpression(struct Parser *parser);

void ParserMatch(struct Parser *parser, enum TokenKind kind)
{
	if (parser->lookahead.kind != kind)
	{
		LONG n = LexerGetBlockNum(parser->lexer) + 1;
		printf("syntax error: token %ld is not %ld (%s:%ld)\n", parser->lookahead.kind, kind, parser->filePath, n);
	}
	
	parser->lookahead = LexerNext(parser->lexer);
	
	if (parser->lookahead.kind == TOKEN_DEFINE)
		ParseDefine(parser);
	else if (parser->lookahead.kind == TOKEN_INCLUDE)
		ParseInclude(parser);
	else if (parser->lookahead.kind == TOKEN_HASH)
		ParserMatch(parser, TOKEN_HASH);
}

void ParseScalarType(struct Parser *parser)
{
	parser->currentType = 0;
	ParserMatch(parser, TOKEN_SCALARTYPE);
}

BOOL matchAwayKeyword(struct Parser *parser)
{
	STRPTR t = parser->lookahead.text;

	if (parser->lookahead.kind != TOKEN_SYMBOL)
		return FALSE;

	
	if (t[0] == '_' && t[1] == '_')
	{
		t += 2;
		
		if (strcmp(t, "saveds") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "asm") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "stdargs") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "stkargs") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "far") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "chip") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "fast") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "interrupt") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if (strcmp(t, "amigainterrupt") == 0)
			ParserMatch(parser, TOKEN_SYMBOL);
		else if ((t[0] == 'a' || t[0] == 'd')
					 && t[2] == 0 && isdigit(t[1]))
		{
			ParserMatch(parser, TOKEN_SYMBOL); // eg __a1
		}
		else if (strcmp(t, "reg") == 0)
		{
			ParserMatch(parser, TOKEN_SYMBOL); // __reg
			ParserMatch(parser, TOKEN_OPENPAR);
			ParserMatch(parser, TOKEN_STRING); // eg "a0"
			ParserMatch(parser, TOKEN_COMMA);
			ParserMatch(parser, TOKEN_CLOSEPAR);
		}
		else
			return FALSE;
		
		return TRUE;
	}

	return FALSE;
}

int ParseSymbol(struct Parser *parser, SymbolRef partOf, SymbolRef type, ULONG kind, ULONG blockNum)
{
	int numPar = 0;
	
	while (TRUE)
	{
		if (parser->lookahead.kind == TOKEN_OPENPAR)
		{
			numPar++;
			ParserMatch(parser, TOKEN_OPENPAR);
		}
		else if (parser->lookahead.kind == TOKEN_MULT)
			ParserMatch(parser, TOKEN_MULT);
		else
			if (! matchAwayKeyword(parser))
				break;
	}
	
	if (parser->lookahead.kind == TOKEN_SYMBOL)
	{
		// we have a global or function
		STRPTR symName = parser->lookahead.text;
		SymbolRef symref;
		
		ParserMatch(parser, TOKEN_SYMBOL);
		
		while (TRUE)
		{
			if (parser->lookahead.kind == TOKEN_OPENSQUARE)
			{
				ParserMatch(parser, TOKEN_OPENSQUARE);
	
				while (parser->lookahead.kind != TOKEN_CLOSESQUARE)
				{
					if (parser->lookahead.kind == TOKEN_END)
						return FALSE;
						
					ParserMatch(parser, parser->lookahead.kind);
				}
				
				ParserMatch(parser, TOKEN_CLOSESQUARE);
			}
			else if (numPar && parser->lookahead.kind == TOKEN_CLOSEPAR)
			{
				numPar--;
				ParserMatch(parser, TOKEN_CLOSEPAR);
			}
			else
				break;
		}
		
		if (numPar)
			return FALSE;
		
		symref = AddSymbol(parser->sdb, symName, partOf, type, kind, blockNum);
		
		if (parser->lookahead.kind == TOKEN_OPENPAR)
		{
			SymbolRef outerScope = parser->currentScope;
			
			parser->currentScope = symref;
			
			do
			{
				ParserMatch(parser, TOKEN_OPENPAR);
				numPar++;
			} while(parser->lookahead.kind == TOKEN_OPENPAR);

			while (TRUE)
			{
				if (parser->lookahead.kind == TOKEN_CLOSEPAR)
					break;
				
				ParseDecl(parser, FALSE, SDB_INSTANCE);

				if (parser->lookahead.kind == TOKEN_COMMA)
					ParserMatch(parser, TOKEN_COMMA);
				
				if (parser->lookahead.kind == TOKEN_END)
					return FALSE;
			}
			while (numPar--)
			{
				if (parser->lookahead.kind == TOKEN_END)
					return FALSE;
				ParserMatch(parser, TOKEN_CLOSEPAR);
			}
			if (parser->lookahead.kind == TOKEN_OPENCURLY)
			{				
				ChangeSymbolKind(parser->sdb, symref, SDB_SCOPE);
				ParseBlock(parser);
			}
			else
				DeleteTrailingSymbols(parser->sdb, symref);

			parser->currentScope = outerScope;
		}
		return TRUE;
	}
	return FALSE;
}

int ParseEnumDeclaration(struct Parser *parser)
{
	SymbolRef declType = parser->declarationType;
	int retval;
	
	if (parser->lookahead.kind != TOKEN_SYMBOL)
		return FALSE;

	retval = ParseSymbol(parser, declType, 0, SDB_MEMBER, parser->lookahead.blockNum);

	while ((parser->lookahead.kind != TOKEN_COMMA) &&
		(parser->lookahead.kind != TOKEN_CLOSECURLY) &&
		(parser->lookahead.kind != TOKEN_END))
		ParserMatch(parser, parser->lookahead.kind);
	
	return parser->lookahead.kind == TOKEN_COMMA;
}

void ParseCompound(struct Parser *parser, BOOL isEnum)
{
	STRPTR name = NULL;
	ULONG blockNum = parser->lookahead.blockNum;
	
	ParserMatch(parser, parser->lookahead.kind); // struct,enum,union

	if (parser->lookahead.kind == TOKEN_SYMBOL)
	{
		// we have an identifier
		name = parser->lookahead.text;
		ParserMatch(parser, TOKEN_SYMBOL);
	}
	
	if (parser->lookahead.kind == TOKEN_OPENCURLY)
	{
		// we have a struct-declaration
		SymbolRef prevDeclType = parser->declarationType;
		parser->declarationType = AddSymbol(parser->sdb, name,
					 parser->currentScope, 0, SDB_COMPOUND, blockNum);
		
		ParserMatch(parser, TOKEN_OPENCURLY);
		
		if (isEnum)
			while (ParseEnumDeclaration(parser))
				ParserMatch(parser, TOKEN_COMMA);
		else
			while (ParseDecl(parser, TRUE, SDB_MEMBER))
				ParserMatch(parser, TOKEN_SEMICOLON);
				
		parser->currentType = parser->declarationType;
		ParserMatch(parser, TOKEN_CLOSECURLY);
		parser->declarationType = prevDeclType;
	}
	else
	{
		SymbolRef scopeRefs[2];
		
		scopeRefs[0] = parser->fileScope;
		scopeRefs[1] = EndSymbolRef;
		// struct defined elsewhere this is just a ref but we need to look it up
		parser->currentType =  NamedCompound(parser->sdb,
				name, scopeRefs);
	}
}

void ParseTypeDef(struct Parser *parser)
{
	ULONG blockNum = parser->lookahead.blockNum;
	SymbolRef targetType = 0;
	int usign = FALSE;
	
	ParserMatch(parser, TOKEN_TYPEDEF);

	if (parser->lookahead.kind == TOKEN_CONST)
		ParserMatch(parser, TOKEN_CONST);

	if (parser->lookahead.kind == TOKEN_UNSIGNED)
	{
		usign = TRUE;
		ParserMatch(parser, TOKEN_UNSIGNED);
	}
	
	if (usign || parser->lookahead.kind == TOKEN_SCALARTYPE)
	{
		targetType = 0;
		if (parser->lookahead.kind == TOKEN_SCALARTYPE)
			ParserMatch(parser, TOKEN_SCALARTYPE);
	}
	else if (parser->lookahead.kind == TOKEN_SYMBOL)
	{
		SymbolRef scopeRefs[2];
		
		scopeRefs[0] = parser->fileScope;
		scopeRefs[1] = EndSymbolRef;
		// refering to a typedef, so we need to look it up
		targetType =  NamedType(parser->sdb,
				parser->lookahead.text, scopeRefs);

		ParserMatch(parser, TOKEN_SYMBOL);
	}
	else if (parser->lookahead.kind == TOKEN_STRUCTORUNION)
	{
		ParseCompound(parser, FALSE);
		targetType = parser->currentType;
	}
	else if (parser->lookahead.kind == TOKEN_ENUM)
	{
		ParseCompound(parser, TRUE);
		targetType = parser->currentType;
	}
	
	while (parser->lookahead.kind != TOKEN_SEMICOLON &&
		parser->lookahead.kind != TOKEN_END)
	{
		if (parser->lookahead.kind == TOKEN_SYMBOL)
		{
			// we have a new type name
			AddSymbol(parser->sdb, parser->lookahead.text, parser->currentScope, targetType, SDB_TYPE, blockNum);
		}		
		ParserMatch(parser, parser->lookahead.kind);
	}
}

void ParseDefine(struct Parser *parser)
{
	ULONG blockNum = parser->lookahead.blockNum;
	SymbolRef ref;
	STRPTR split;
	STRPTR args = NULL;
	
	//Need to add a null char to split the key and the value
	// We can safely do that as the text is in the stringstorage
	split = parser->lookahead.text;
	
	while (*split && !isspace(*split) && *split != '(')
		split++;

	// now split is guaranteed to be the first if it is there at all
	if (*split)
	{
		if (*split == '(')
		{
			*split++ = 0;
			args = split;
			while (*split && *split != ')')
				split++;
			if (*split)
				*split++ = 0; // terminate arg string & eat the )
		}
		else
			*split++ = 0;

		while (isspace(*split))
			split++;
	}

	ref = AddSymbol(parser->sdb, parser->lookahead.text, parser->fileScope, 0, SDB_DEFINITION, blockNum);
	
	StoreDefinitionValue(parser->sdb, ref, *split ? split : (STRPTR)"");
	if (args)
		StoreDefinitionArgs(parser->sdb, ref, args);
	
	ParserMatch(parser, TOKEN_DEFINE);
}

// filePath is modified to the full path if we find it
BPTR LockIncludeFile(STRPTR filePath, STRPTR immediatePath, BPTR tmpDir, STRPTR searchPaths[])
{
	TEXT pathBuffer[1024];
	BPTR fileLock = NULL;
	
	Strncpy(pathBuffer, immediatePath, sizeof(pathBuffer));
	AddPart(pathBuffer, filePath, sizeof(pathBuffer));
	
	if (tmpDir)
	{
		BPTR curDir = CurrentDir(tmpDir);
		
		if (fileLock = Lock(pathBuffer, SHARED_LOCK))
			strcpy(filePath, pathBuffer);
			
		if (! fileLock)
			fileLock = Lock(filePath, SHARED_LOCK);
	
		CurrentDir(curDir);
	}
	if (!fileLock && (fileLock = Lock(pathBuffer, SHARED_LOCK)))
		strcpy(filePath, pathBuffer);
		
	if (! fileLock)
		fileLock = Lock(filePath, SHARED_LOCK);
	
	while (! fileLock && *searchPaths != NULL)
	{
		Strncpy(pathBuffer, *searchPaths, sizeof(pathBuffer));
		AddPart(pathBuffer, filePath, sizeof(pathBuffer));
		fileLock = Lock(pathBuffer, SHARED_LOCK);
		if (fileLock)
		{
			strcpy(filePath, pathBuffer);
			break;
		}
		searchPaths++; 
	}
	
	return fileLock;
}

void ParseInclude(struct Parser *parser)
{
	STRPTR path = parser->lookahead.text;
	STRPTR cptr;
	UWORD fileRef = NoFileRef;
	TEXT actualPath[1024];
	struct FileInfoBlock *fib;
		
	if (*path == '<')
	{
		path++;
		cptr = path;
		while (*cptr && *cptr != '>')
			cptr++;
		*cptr = 0;
	}
	else if (*path == '"')
	{
		path++;
		cptr = path;
		
		while (*cptr && *cptr != '"')
			cptr++;
			
		*cptr = 0;
	}
	
	strcpy(actualPath, path);
	
	fib = AllocDosObject(DOS_FIB, NULL);
	if (fib)
	{
		BPTR fileLock;
		fileLock = LockIncludeFile(actualPath, parser->filePath, parser->tmpDir, parser->auxInfo->searchPaths);
		
		if (fileLock)
		{
			Examine(fileLock, fib);
			parser->auxInfo->fileSize = fib->fib_Size;
			parser->auxInfo->dateStamp = &fib->fib_Date;
			
			fileRef = FindFileID(parser->sdb, actualPath);
			if ((fileRef == NoFileRef) || IsObsolete(parser->sdb, fileRef, fib, ! parser->auxInfo->NDKMode))
				fileRef = ParseFileImpl(parser->sdb, fileLock, actualPath, parser->tmpDir, parser->auxInfo, fileRef);
			else
				UnLock(fileLock);
		}

		FreeDosObject(DOS_FIB, fib);
	}
			
	if (fileRef != NoFileRef)
		AddInclude(parser->sdb, parser->fileScope, fileRef);

	ParserMatch(parser, TOKEN_INCLUDE);	
}

int ParseDecl(struct Parser *parser, BOOL allowCommas, ULONG sdbKind)
{
	ULONG blockNum = parser->lookahead.blockNum;
	SymbolRef targetType = EndSymbolRef;
	int retval;
	int nextSymIsInstance = FALSE;
	SymbolRef scope = (sdbKind == SDB_MEMBER) ? parser->declarationType : parser->currentScope;
	
	while (TRUE)
	{
		if (matchAwayKeyword(parser))
			;
		else if (parser->lookahead.kind == TOKEN_CONST)
		{
			ParserMatch(parser, TOKEN_CONST);
		}
		else if (parser->lookahead.kind == TOKEN_UNSIGNED)
		{
			targetType = 0;
			ParserMatch(parser, TOKEN_UNSIGNED);
		}
		else if (parser->lookahead.kind == TOKEN_SYMBOL)
		{
			SymbolRef scopeRefs[2];
			SymbolRef type;
			
			//NOTE this following test for: unsigned LONG sym
			// even more for: usinged NOTYETDEFFED sym
			// remember we need to support: NOTYETDEFFED sym
			if (targetType != EndSymbolRef || nextSymIsInstance)
				break; // this symbol is our instance. Can't be a typedef
			
			nextSymIsInstance = TRUE;
			
			scopeRefs[0] = parser->fileScope;
			scopeRefs[1] = EndSymbolRef;
			// refering to a typedef, so we need to look it up
			type = NamedType(parser->sdb,
					parser->lookahead.text, scopeRefs);
	
			ParserMatch(parser, TOKEN_SYMBOL);

			targetType = type;
		}
		else if (parser->lookahead.kind == TOKEN_STRUCTORUNION)
		{
			ParseCompound(parser, FALSE);

			targetType = parser->currentType;
			nextSymIsInstance = TRUE;
		}
		else if (parser->lookahead.kind == TOKEN_ENUM)
		{
			ParseCompound(parser, TRUE);

			targetType = parser->currentType;
			nextSymIsInstance = TRUE;
		}
		else if (parser->lookahead.kind == TOKEN_SCALARTYPE)
		{
			targetType = 0;
			ParserMatch(parser, TOKEN_SCALARTYPE);
		}
		else if (parser->lookahead.kind == TOKEN_MULT)
			ParserMatch(parser, TOKEN_MULT);
		else
			break;
	}

	if (parser->lookahead.kind == TOKEN_PERIOD)
	{
		ParserMatch(parser, TOKEN_PERIOD);
		
		if (parser->lookahead.kind != TOKEN_PERIOD)
			return FALSE;
		ParserMatch(parser, TOKEN_PERIOD);
		
		if (parser->lookahead.kind != TOKEN_PERIOD)
			return FALSE;
		ParserMatch(parser, TOKEN_PERIOD);

		return TRUE;
	}

	do
	{
		retval = ParseSymbol(parser, scope, targetType, sdbKind, blockNum);

		if (parser->lookahead.kind == TOKEN_COLON)
		{
			ParserMatch(parser, TOKEN_COLON);
			if (parser->lookahead.kind == TOKEN_NUMBER)
				ParserMatch(parser, TOKEN_NUMBER);
		}

		if ((parser->lookahead.kind == TOKEN_SYMBOL) &&
			strcmp(parser->lookahead.text, "__asm") == 0)
		{
			ParserMatch(parser, TOKEN_SYMBOL); // __asm
			ParserMatch(parser, TOKEN_OPENPAR);
			ParserMatch(parser, TOKEN_STRING); // eg "a0"
			ParserMatch(parser, TOKEN_COMMA);
			ParserMatch(parser, TOKEN_CLOSEPAR);
		}
			
		if (parser->lookahead.kind == TOKEN_EQUAL)
		{
			ParserMatch(parser, TOKEN_EQUAL);
			ParseExpression(parser);
		}

		if (! allowCommas)
			break;
			
		if (parser->lookahead.kind != TOKEN_COMMA)
			break;

		ParserMatch(parser, TOKEN_COMMA);

		while (TRUE)
		{
			if (matchAwayKeyword(parser))
				;
			else if (parser->lookahead.kind == TOKEN_MULT)
			{
				ParserMatch(parser, TOKEN_MULT);
			}
			else
				break;
		}
	} while (retval && parser->lookahead.kind != TOKEN_SEMICOLON);
	
	return retval;	
}

int ParseExtDecl(struct Parser *parser)
{
	if (ParseDecl(parser, TRUE, SDB_INSTANCE))
		;
	else if (parser->lookahead.kind == TOKEN_TYPEDEF)
		ParseTypeDef(parser);
	else
		return FALSE;

	if (parser->lookahead.kind == TOKEN_SEMICOLON)
		ParserMatch(parser, TOKEN_SEMICOLON);
		
	return TRUE;
}

void ParseControlPars(struct Parser *parser)
{
	int parLevel = 0;
	
	if (parser->lookahead.kind != TOKEN_OPENPAR)
		return;
		
	do
	{
		if (parser->lookahead.kind == TOKEN_OPENPAR)
			parLevel++;
		else if (parser->lookahead.kind == TOKEN_CLOSEPAR)
			parLevel--;
		else if (parser->lookahead.kind == TOKEN_END)
			return;

		ParserMatch(parser, parser->lookahead.kind);
	}
	while (parLevel>0);
}

void ParseExpression(struct Parser *parser)
{
	int parCount = 0;
	int curlyCount = 0;
	while (TRUE)
	{
		enum TokenKind kind = parser->lookahead.kind;
		
		if (kind == TOKEN_OPENPAR)
			parCount++;
		else if (kind == TOKEN_CLOSEPAR)
			parCount--;
		else if (kind == TOKEN_OPENCURLY)
			curlyCount++;
		else if (kind == TOKEN_CLOSECURLY)
		{
			if (curlyCount)
				curlyCount--;
			else
				return;
		}
		else if (kind == TOKEN_END)
			return;
		
		if (!parCount && !curlyCount)
		{
			if (kind == TOKEN_SEMICOLON)
				return;			
			if (kind == TOKEN_COMMA)
				return;
		}	

		// just eat it
		ParserMatch(parser, parser->lookahead.kind);
	}
}

void ParseStatement(struct Parser *parser)
{
	while (TRUE)
	{
		enum TokenKind kind = parser->lookahead.kind;
		
		if (kind == TOKEN_CLOSECURLY)
			return;

		if (kind == TOKEN_END)
			return;

		if (kind == TOKEN_CONTROLSTATEMENT)
		{
			ParserMatch(parser, TOKEN_CONTROLSTATEMENT);
			ParseControlPars(parser);
			return;
		}

		if (kind == TOKEN_DO)
		{
			ParserMatch(parser, TOKEN_DO);
			return;
		}
		
		// just eat it
		ParserMatch(parser, parser->lookahead.kind);

		if (kind == TOKEN_SEMICOLON) // bail out after semicolon (but eat it first)
			return;			
	}
}

void ParseBlock(struct Parser *parser)
{
	ParserMatch(parser, TOKEN_OPENCURLY);

	while (parser->lookahead.kind != TOKEN_CLOSECURLY)
	{
		if (parser->lookahead.kind == TOKEN_END)
			return;

		if (parser->lookahead.kind == TOKEN_OPENCURLY)
		{
			SymbolRef outerScope = parser->currentScope;
			
			parser->currentScope = AddSymbol(parser->sdb, NULL, outerScope, 0, SDB_SCOPE,
				 parser->lookahead.blockNum);
				 
			ParseBlock(parser);
			
			parser->currentScope = outerScope;
		}
		else if (!ParseExtDecl(parser))
			ParseStatement(parser);
	}

	AddSymbol(parser->sdb, NULL, parser->currentScope, 0, SDB_SCOPEEND, parser->lookahead.blockNum);

	ParserMatch(parser, TOKEN_CLOSECURLY);
}


UWORD ParseFileImpl(APTR sdb, BPTR fileLock, STRPTR filePath, BPTR tmpDir, struct ParseAuxInfo *pai, ULONG fileID)
{
	struct Parser parser;
	int bufSize;
	STRPTR buffer;
	BPTR file;
	UWORD retval = NoFileRef;

	if ((Strnicmp(filePath, "inline/",7) == 0)
		||(Strnicmp(filePath, "pragma/",7) == 0)
		||(Strnicmp(filePath, "pragmas/",8) == 0)
		)
	{
		UnLock(fileLock);
		
		return retval;
	}

	file = OpenFromLock(fileLock);
	if (! file)
	{
		UnLock(fileLock);
		
		Printf("Couldn't open %s\n", filePath);
		return retval;
	}
	
	if (pai->verbosity > 0)
		Printf("Parsing %s\n", filePath);
		
	bufSize = pai->fileSize + 2;
	
	buffer = AllocMem(bufSize, MEMF_ANY);
	buffer[Read(file, buffer, bufSize)] = 0;
	Close (file);
	
	// start global scope
	parser.currentScope = StartCodeFile(sdb, filePath, pai->fileSize, pai->dateStamp, pai->NDKMode, fileID);
	if ((parser.currentScope & 0xFFFF0000) != 0xFFFF0000)
	{
		parser.fileScope = parser.currentScope;
	
		parser.sdb = sdb;
		parser.auxInfo = pai;
		parser.lexer = NewLexer(buffer, sdb, parser.fileScope);
		parser.lookahead.kind = TOKEN_START;
		parser.filePath = filePath;
		parser.tmpDir = tmpDir;
	
		ParserMatch(&parser, TOKEN_START);
		
		while (parser.lookahead.kind != TOKEN_END)
		{
			if (!ParseExtDecl(&parser))
				ParseStatement(&parser);
			if (parser.lookahead.kind == TOKEN_CLOSECURLY)
				ParserMatch(&parser, TOKEN_CLOSECURLY);
		}
		// end global scope	
		retval = FinalizeCodeFile(parser.sdb, parser.fileScope, parser.lookahead.blockNum);

		DeleteLexer(parser.lexer);
	}
	
	FreeMem(buffer, bufSize);
		
	return retval;
}

ULONG sizeOfFile(BPTR lock)
{
	struct FileInfoBlock *fib;
	ULONG size;

	fib = AllocDosObject(DOS_FIB, TAG_DONE);
	
	if (! fib)
		return 0;

	Examine(lock, fib);
	
	size = fib->fib_Size;

	FreeDosObject(DOS_FIB, fib);

	return size;
}

UWORD __ASM__ ParseFile(__REG__(a0, APTR sdb), __REG__(a1, STRPTR projRelFilePath), __REG__(a2, BPTR tmpDir), __REG__(a3, struct ParseAuxInfo *pai))
{
	BPTR fileLock = NULL;
	ULONG fileRef;
	
	if (tmpDir)
	{
		BPTR curDir = CurrentDir(tmpDir);
		
		fileLock = Lock(projRelFilePath, SHARED_LOCK);
	
		CurrentDir(curDir);
	}
		
	if ( ! fileLock)
		fileLock = Lock(projRelFilePath, SHARED_LOCK);

	if (! fileLock)
		Printf("  Unable to lock %s\n", projRelFilePath);

	if (! fileLock)
		return NoFileRef;

	fileRef = FindFileID(sdb, projRelFilePath);

	if ((fileRef == NoFileRef) || IsObsolete(sdb, fileRef, NULL, ! pai->NDKMode))
	{
		if (pai->fileSize == 0)
			pai->fileSize = sizeOfFile(fileLock);
		return ParseFileImpl(sdb, fileLock, projRelFilePath, tmpDir, pai, fileRef);
	}
		
	Printf("%s skipped\n", projRelFilePath);
	return NoFileRef;
}
