#include <exec/types.h>

#include <containerkit/array.h>
#include <containerkit/map.h>

#include "symdb.h"

struct SymbolRefParts
{
	UWORD file;
	UWORD symbol;
};

#define SRFile(x) (((struct SymbolRefParts *)&(x))->file)
#define SRSym(x) (((struct SymbolRefParts *)&(x))->symbol)

struct Symbol
{
	STRPTR name;
	SymbolRef partOf;
	SymbolRef typeId;
	UWORD blockNum;
	UBYTE columnNum;
	UBYTE kind;
};

/*
instances specify typeid to a compound typedef, or 0 if simple
instances can be partOf a  scope

compounds specify typeid to itself
compounds can be partOf a compound typedef or scope

members specify  typeid to a compound typedef, or 0 if simple
members are partOf a compound typedef

scope (block/function) always have typeid 0
scope can be partOf another scope or 0 if global scope
block scopes have name NULL
*/


struct FileSymbols
{
	ULONG fileSize;
	struct DateStamp fileDate;
	ULONG staticContents;
	struct Symbol *symbols;
	int numSymbols;
	STRPTR filePath;
	UWORD *includes; // file IDs NoFileRef terminated
	SymbolRef *scopeStarts; // EndSymbolRef terminated
	SymbolRef *sortedScopeEnds; // EndSymbolRef terminated
	UWORD fileID;
	STRPTR strings;
	Array stringsArray;
	Array includesArray;
	Array scopeStartsArray;
	Array sortedScopeEndsArray;
	Map definitionValues; //symbolRef -> definition value
	Map definitionArgs; //symbolRef -> definition argument string
	BPTR codeIndexDirLock;
	Array nameMap;
};
/*
-I NDK:include_h/

Work:a/include_H/c/d/fhdgh.h

work:devel/ndk/include_h/
  codeindex/
    0001.db
    0011.db
	areaname		NDK:include_h/
	highestfileid   0011
    nametoid.map    exec/memory.h -> 0001

Codeindex:fileAreas			0x000_	0
                            0x001_	15
                            0x002_	30
Codeindex:fileAreaNames		NDK:include_h/
                            NDK:include_h/
                            myprogram

myprogram/
  main.c
  codeindex/
   0021.db
   0022.db
   nametoid.map		work:a/b/c/d/fhdgh.h -> 0021
					main.c -> 0022
   

when parsing an #include we start by Lock() the file and walk up
the dir tree until we find a companion codeindex drawer.
If it exists we have found the "project".
If we get all the way up and no codeindex drawer is found we will
say that the files belongs to the current "project"

we will then look up the file in the nametoid.map and from there
open the eg 147.db file
If we couldn't look up a new db file will have to be created, and
to do that we first need to figure out an id for it. We look in
the highestfileid file and increment. But if it ends with 0xF we need
a new area. The new area will just be appended to the fileAreas
file and with a name as found in the codeindex/areaname file.

When creating a new codeindex drawer the areaname needs to be created
as well (as specified by the user)

nametoid.map has absolute paths or relative to "project"

When doing codecompletion we would be able to go from a SymbolRef
to a file area by looking in the fileIdAreas file and from there
simply opening the db file


TODO:
 make command run on NDK
  - timestamp and size stored in *.db
  - also store a boolean: StaticContents (for ndk like files)

 - fs->symbols should be array too during parsing
 - lexer of undef
 - parser of initializers
 - in memory cache of .db files
 - Use columnNum in addition to BlockNum
 
 - namedCompound() should look up using a hashmap (not looping)
 - be failure resistent in case user types rubbish
*/

  
struct SymDb
{
	Map fileSymsByFilename; // those in memory
	Map fileSymsByID;       // those in memory
	
	Map lookupRunMap;
	LONG lookupRunDepth;
	LONG verbosity;
};
